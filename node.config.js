const config = {
  // base settings
  PORT: 5500,
  FRONT_PATH: `client`,
  ROOT_PATH: __dirname,

  // API config
  ROUTER_BASE: `/`,
  API_PREFIX: `/api`,

  // database
  SQLITE_DB_FOLDER: `database`,
  SQLITE_DB: `colibri.sqlite`,
  SQLITE_DB_MAX_POOL: 1,

  // auth (keycloak)
  EXPRESS_COOKIE_SECURE: false,
  EXPRESS_SESSION_SECRET: `<fill-in-your-secret>`,
  KEYCLOAK_REALM: `master`,
  KEYCLOAK_ROLE: `colibri`,
  KEYCLOAK_ADMIN_URL: `http://localhost:8080`,
  KEYCLOAK_CLIENT_ID: `colibri`,

  // resources & assets
  CONTENT_FOLDER: `static/content`,
  MAX_PAYLOAD_SIZE: `150MB`,
  
  // debug and logging
  LOGGER_LEVEL: 3,
};

config.SQLITE_DB_FOLDER = process.env.SQLITE_DB_FOLDER || config.SQLITE_DB_FOLDER;
config.SQLITE_DB = process.env.SQLITE_DB || config.SQLITE_DB;
config.SQLITE_DB_MAX_POOL = parseInt(process.env.SQLITE_DB_MAX_POOL) || config.SQLITE_DB_MAX_POOL;

config.EXPRESS_SESSION_SECRET = process.env.EXPRESS_SESSION_SECRET || config.EXPRESS_SESSION_SECRET;
config.KEYCLOAK_ADMIN_URL = process.env.KEYCLOAK_ADMIN_URL || config.KEYCLOAK_ADMIN_URL;
config.KEYCLOAK_CLIENT_ID = process.env.KEYCLOAK_CLIENT_ID || config.KEYCLOAK_CLIENT_ID;
config.KEYCLOAK_CLIENT_SECRET = process.env.KEYCLOAK_CLIENT_SECRET;
config.KEYCLOAK_CLIENT_UUID = process.env.KEYCLOAK_CLIENT_UUID;
config.KEYCLOAK_REALM = process.env.KEYCLOAK_REALM || config.KEYCLOAK_REALM;

config.MAX_PAYLOAD_SIZE = process.env.MAX_PAYLOAD_SIZE || config.MAX_PAYLOAD_SIZE;

config.NODE_ENV = config.NODE_ENV || process.env.NODE_ENV || `development`;
config.IS_DEV = config.NODE_ENV === `development`;

if (!config.IS_DEV) {
  config.LOGGER_LEVEL = 0;
}

module.exports = config;
