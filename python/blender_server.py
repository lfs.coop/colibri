# Web server inside Blender, part of Colibri
# Copyright (C) 2015-2020 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


import bpy
from bpy.app.handlers import persistent
from bpy.ops import op_as_string
import json
import uuid
import os.path
import queue
import threading

# Specify here your lib path for ws4py if not properly done
import sys

PYTHON_PACKAGES_DIR = '/Users/mina/.local/share/virtualenvs/python-50aWRkq7/lib/python3.8/site-packages'
# PYTHON_PACKAGES_DIR = "/usr/local/lib/python2.7/site-packages/"
if not PYTHON_PACKAGES_DIR in sys.path:
        sys.path.append(PYTHON_PACKAGES_DIR)


from wsgiref.simple_server import make_server
from ws4py.websocket import WebSocket as _WebSocket
from ws4py.server.wsgirefserver import WSGIServer, WebSocketWSGIRequestHandler
from ws4py.server.wsgiutils import WebSocketWSGIApplication



bl_info = {
    "name": "LFS Webserver",
    "author": "Les Fées Spéciales",
    "version": (0, 0, 1),
    "blender": (2, 80, 0),
    "description": "Connect to a web service outside Blender, such as a pose library.",
    "category": "Pipeline",
    "support": "COMMUNITY",
}

CLASSES = []


# Default is localhost only
allow_list = ['127.0.0.1']

# If False, any connection out of allow_list refused.
# If True, prompted to accept (not yet)
OPEN_SOCKET = False

port = 8137      # Server default port
port_range = 10  # Number of tried ports incrementing from 'port' variable


# Need to store various stuff. Do not change:
wserver = None
wserver_thread = None
message_queue = queue.Queue()
sockets = []
callbacks = {}


####### UTILS

def send_callback(instance, msg={}):
    msg['operator'] = instance.bl_idname
    bpy.ops.lfs.message_callback(callback_idx=instance.callback_idx,
                                 callback_uid=instance.callback_uid,
                                 message=json.dumps(msg))

def send_error(instance, msg={}):
    msg['errored'] = True
    send_callback(instance, msg)


def register_callback(callback):
    '''Register a callback function with an assigned uuid
       used to enable callbacks from operators to the socket'''

    k = str(uuid.uuid4())
    callbacks[k] = callback
    return k


class WebSocketApp(_WebSocket):
    '''This class handles the websockets opening,
       remote closing and message reception'''

    def opened(self):
        print("New connection opened from: %s" % self.peer_address[0])
        if self.peer_address[0] not in allow_list and OPEN_SOCKET is False:
            print("Incoming connection from %s refused" % self.peer_address[0])
            self.close(code=1008, reason="Connection not allowed")
            return

        sockets.append(self)

    def closed(self, code, reason=None):
        print('Connection from %s closed %i: %s'
                    % (self.peer_address[0], code, reason))
        sockets.remove(self)

    def received_message(self, message):
        print("Incoming message from %s" % self.peer_address[0])
        # Queing the message (full) and the socket used
        message_queue.put((message.data.decode(message.encoding), self))


def start_server(op, host, port):
    '''Start a webserver'''

    global wserver, wserver_thread
    if wserver:
        print("Server already running?")
        op.report({'WARNING'}, "Server already running?")
        return False

    source_port = port
    while not wserver or source_port + port_range <= port:
        print("Starting server on port %i" % port)

        op.report({'INFO'}, "Starting server on port %i" % port)
        try:
            wserver = make_server(host, port, server_class=WSGIServer,
                                  handler_class=WebSocketWSGIRequestHandler,
                                  app=WebSocketWSGIApplication(handler_cls=WebSocketApp)
            )
            wserver.initialize_websockets_manager()
        except:
            print("Unable to start the server on port %i" % port)
            op.report({'WARNING'}, "Unable to start the server on port %i" % port)
            port += 1
            if source_port + port_range <= port:
                print("Tried all ports without finding one available")
                op.report({'WARNING'}, "Tried all ports without finding one available")
                return

    wserver_thread = threading.Thread(target=wserver.serve_forever)
    wserver_thread.daemon = True
    wserver_thread.start()
    return True


def stop_server(op):
    '''Stopping the webserver, closing the sockets, ...'''

    global wserver, wserver_thread
    if not wserver:
        return False

    for socket in sockets:
        socket.close(code=1001)
    wserver.shutdown()
    wserver_thread.join()
    wserver_thread._stop()  # not documented but working well
    wserver = None
    # TODO: it appears the threading is still locking something
    # and don't allow blender to properly shutdown
    # FIXME FIX ME FIX PLEASE
    print("Stopped server\n")
    op.report({'INFO'}, "Stopped server")
    return True


# TODO: Move to top?
is_server_running = False

class LFS_OT_start_server(bpy.types.Operator):
    '''Simple operator to start the server'''
    bl_idname = "lfs.start_server"
    bl_label = "LFS: Start Server"

    # port = bpy.props.IntProperty()
    # host = bpy.props.StringProperty()

    def modal(self, context, event):
        if not is_server_running:
            print("Stopping server")
            stop_server(self)
            self.cancel(context)
            return{'FINISHED'}

        elif event.type == 'TIMER':
            while not message_queue.empty():
                message, socket = message_queue.get()
                print("NEW MESSAGE")
                try:
                    callback_idx = register_callback(lambda callback_msg: socket.send(callback_msg))
                    bpy.ops.lfs.message_dispatcher(message=message, callback_idx=callback_idx)
                except Exception as e:
                    print("MESSAGE ERROR: " + str(e))

        return {'PASS_THROUGH'}

    def cancel(self, context):
        wm = context.window_manager
        wm.event_timer_remove(self._timer)

    def execute(self, context):
        global is_server_running
        if is_server_running:
            is_server_running = False
            return {'FINISHED'}
        else:
            port = context.scene.lfs_port
            host = context.scene.lfs_host
            start_server(self, host, port)

            # create timer
            wm = context.window_manager
            self._timer = wm.event_timer_add(0.1, window=context.window)
            wm.modal_handler_add(self)
            is_server_running = True
            return {'RUNNING_MODAL'}


class LFS_OT_message_callback(bpy.types.Operator):
    '''Used to send messages back to the used socket'''

    bl_idname = "lfs.message_callback"
    bl_label = "LFS: Message Callback"

    callback_idx: bpy.props.StringProperty()
    callback_uid: bpy.props.StringProperty()
    message: bpy.props.StringProperty()

    def execute(self, context):
        self.message = json.dumps({"event": self.callback_uid, "data": self.message})
        print("Message Call Back to %s: %s" % (self.callback_idx, self.message))
        callbacks[self.callback_idx](self.message)
        del callbacks[self.callback_idx]  # Callback can only be used once
        return {'FINISHED'}


def cleanup_keys(op, msg):
    props = op.get_rna_type().properties
    clean_msg = {}
    for key in msg:
            if key in props:
                    clean_msg[key] = msg[key]
    return clean_msg


class LFS_OT_message_dispatcher(bpy.types.Operator):
    '''The main operator that gets the messages and checks them before exec
    So far, for security reasons, you can only call registered operators,
    custom ones (from add-ons, or your scripts) or any original ones in Blender.'''

    bl_idname = "lfs.message_dispatcher"
    bl_label = "LFS: Message Dispatcher"

    message: bpy.props.StringProperty()
    callback_idx: bpy.props.StringProperty()

    @staticmethod
    def operator_exists(idname):
        '''Check if an operator exists
        thanks people on blenderartists'''
        try:
            op_as_string(idname)
            return True
        except:
            return False

    # Message should be a json string
    # Contains a least an operator parameter to call
    def execute(self, context):
        self.report({'INFO'}, "Message: %s" % self.message)
        self.report({'INFO'}, "Callback idx: %s\n" % self.callback_idx)

        try:
            # msg SHOULD be a valid json string
            msg = json.loads(self.message)
        except:
            self.report({'ERROR'}, "message is no valid json")
            bpy.ops.lfs.message_callback(callback_idx=self.callback_idx,
                                         callback_uid="",
                                         message="ERROR: message no json")
            return {'CANCELLED'}

        if 'operator' not in msg:
            # msg lib should have an operator key
            self.report({'ERROR'}, "JSON not well formed: no operator specified in json")
            bpy.ops.lfs.message_callback(callback_idx=self.callback_idx,
                                         callback_uid=msg.get("callback_uid", ""),
                                         message="ERROR: no operator specified in json")
            return {'CANCELLED'}

        if not self.operator_exists(msg['operator']):
            # the operator to call needs to be registered before
            self.report({'ERROR'}, "Operator %s not defined" % msg['operator'])
            ops.bpy.lfs.message_callback(callback_idx=self.callback_idx,
                                         callback_uid=msg.get("callback_uid", ""),
                                         message="ERROR, Operator %s not defined" %
                                         msg['operator'])
            return {'CANCELLED'}

        # getting the function you're looking for
        namespace = getattr(bpy.ops, msg["operator"].split(".")[0])
        op = getattr(namespace, msg["operator"].split(".")[1])

        msg["callback_idx"] = self.callback_idx
        msg = cleanup_keys(op, msg)

        # all other keys sent by the app are up to you!

        # calling the operator, providing the message
        op(**msg)

        return {'FINISHED'}


class LFS_OT_blender_ping(bpy.types.Operator):
    '''Simple demo operator that returns current opened file'''

    bl_idname = "lfs.blender_ping"
    bl_label = "LFS: Blender Ping"

    callback_idx: bpy.props.StringProperty()
    callback_uid: bpy.props.StringProperty()

    def execute(self, context):
        msgBack = {'filepath': bpy.data.filepath if bpy.data.filepath else 'untitled',
                             'filename': os.path.basename(bpy.data.filepath) if bpy.data.filepath else 'untitled'}
        send_callback(self, msgBack)
        return {'FINISHED'}

### UI

class LFS_PT_webserver_panel(bpy.types.Panel):
    '''Webserver Panel'''
    bl_label = "Colibri Webserver"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "LFS"


    def draw(self, context):
        layout = self.layout

        scene = context.scene
        col = layout.column(align=True)
        row = col.row()
        row.prop(scene, "lfs_host")
        row.prop(scene, "lfs_port")
        col.operator("lfs.start_server", text="Stop server" if is_server_running else "Start Server", icon='PAUSE' if is_server_running else "PLAY")


CLASSES.extend([
    # main / webserver
    LFS_OT_message_dispatcher,
    LFS_OT_message_callback,
    LFS_OT_blender_ping,
    LFS_OT_start_server,
    LFS_PT_webserver_panel,
])


def register():
    bpy.types.Scene.lfs_host = bpy.props.StringProperty(name="Host", default="localhost", description="The host to connect the PoseLib to")
    bpy.types.Scene.lfs_port = bpy.props.IntProperty(name="Port", default=8137, description="The port to connect the PoseLib to")
    bpy.types.Scene.last_call = bpy.props.FloatProperty()

    for cl in CLASSES:
        bpy.utils.register_class(cl)


def unregister():
    for cl in reversed(CLASSES):
        bpy.utils.unregister_class(cl)

    del bpy.types.Scene.lfs_host
    del bpy.types.Scene.lfs_port
    del bpy.types.Scene.last_call


if __name__ == "__main__":
    register()
