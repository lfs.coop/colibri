# Colibri, copyright (C) 2015-2020 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


import bpy
import os
import tempfile
import base64
import json
from mathutils import Matrix


_initial_pose_data = {}


####### UTILS

def send_callback(instance, msg={}):
    msg['operator'] = instance.bl_idname
    bpy.ops.lfs.message_callback(callback_idx=instance.callback_idx,
                                 callback_uid=instance.callback_uid,
                                 message=json.dumps(msg))


def send_error(instance, msg={}):
    msg['errored'] = True
    send_callback(instance, msg)


def get_selected_bones():
    bone_list = []
    if len(bpy.context.selected_pose_bones):
        bone_list = bpy.context.selected_pose_bones
    else:
        for arma in [r for r in bpy.data.objects if r.type == 'ARMATURE']:
            bone_list.extend(arma.pose.bones)
    return bone_list


def get_pose():
    '''
    This function get the matrix (position/rotation/scale)
    of all selected bones and return it in a dictionary.
    The dictionnary looks like this:
    {'armature_name': {'bone_name':[(),(),(),()],
                       'next_bone':[matrix],
                       ...
                      }
    }
    '''
    bpy.ops.object.mode_set(mode='POSE')
    # TODO: get the active character/set?
    bonesTransform_dict = {}
    bones_list = get_selected_bones()

    bonesTransform_dict = {}
    for bone in bones_list:
        if bone.id_data.name not in bonesTransform_dict:
            bonesTransform_dict[bone.id_data.name] = {}
        matrix_final = bone.matrix_basis
        matrix_json = [tuple(e) for e in list(matrix_final)]

        bonesTransform_dict[bone.id_data.name][bone.name] = matrix_json
    return bonesTransform_dict


def get_selection():
    selection = {}
    bones_list = get_selected_bones()
    for bone in bones_list:
        if bone.id_data.name not in selection:
            selection[bone.id_data.name] = {}
        selection[bone.id_data.name][bone.name] = 0
    return selection


def apply_pose(target_pose_data,
               merge_factor=100,
               initial_merge=False,
               flip=False,
               apply_on_selected=False):
    '''
    This function applies the provided pose dict to the current
    selected armature.
    If a merge factor (float representing a percentage) is provided,
    the initial pose is mixed with the provided one.
    If flip is True, at the end of the process, the flip
    blender option is called to mirror the pose.
    '''
    global _initial_pose_data
    # TODO: Selected armature?
    bpy.ops.object.mode_set(mode='POSE')
    # if merge_factor > -9999:
    merge_factor = float(merge_factor) / 100  # converted to [0,1] value
    if initial_merge or _initial_pose_data == {}:
        _initial_pose_data = get_pose()

    # print(target_pose_data)
    bones = bpy.context.selected_pose_bones
    if bones == []:
        for rig in [r for r in bpy.data.objects if r.type == 'ARMATURE']:
            for bone in rig.pose.bones:
                bones.append(bone)

    # if flip:
    #     tmp_bones = {}
    for bone in bones:
        arma = bone.id_data.name

        # If the armature is in target_pose_data and the bone is in armature
        if target_pose_data.get(arma) and target_pose_data.get(arma).get(bone.name):
            # if merge_factor is None:
            #     json_matrix = Matrix(target_pose_data.get(arma).get(bone.name))
            # else:
            matrix = Matrix(_initial_pose_data[arma][bone.name])
            matrix = matrix.lerp(Matrix(target_pose_data[arma][bone.name]),
                                 merge_factor)
            # matrix *= merge_factor
            # matrix += Matrix(_initial_pose_data.get(arma).get(bone.name)) * (1.0 - merge_factor)
            # matrice = Matrix(matrice)  # Matrix of a matrix object ?

            if flip:
                # Not sure I still need this tmp_bones
                # tmp_bones[bone.name] = bone.matrix_basis.copy()
                # Not sure I should change the selection TOFIX ?
                bone.bone.select = True
            bone.matrix_basis = matrix

    if flip:
        bpy.ops.pose.copy()
        # Not sure it's needed neither
        # for bone, mat in tmp_bones.items():
        #    bpy.context.object.pose.bones[bone].matrix_basis = mat
        bpy.ops.pose.paste(flip=True)
###


def select_bones(json_data):
    bpy.ops.object.mode_set(mode='POSE')
    bpy.ops.pose.select_all(action='DESELECT')
    json_data = json.loads(json_data.decode())
    bones = []
    if bones == []:
        for rig in [r for r in bpy.data.objects if r.type == 'ARMATURE']:
            for bone in rig.pose.bones:
                bones.append(bone)
    for bone in bones:
        arma = bone.id_data.name
        if json_data.get(arma) and json_data.get(arma).get(bone.name):
            bone.bone.select = True


class LFS_OT_colibri_apply_pose(bpy.types.Operator):
    '''Get a pose as a base64-encoded JSON and apply it'''

    bl_idname = "lfs.colibri_apply_pose"
    bl_label = "LFS: Apply pose"

    callback_idx: bpy.props.StringProperty()
    callback_uid: bpy.props.StringProperty()
    pose_b64: bpy.props.StringProperty()
    flip: bpy.props.BoolProperty(default=False)
    apply_on_selected: bpy.props.BoolProperty(default=False)
    # for merging poses
    merge_factor: bpy.props.IntProperty(default=100)
    initial_merge: bpy.props.BoolProperty(default=False)

    def execute(self, context):
        apply_pose(target_pose_data=json.loads(base64.b64decode(self.pose_b64)),
                   initial_merge=self.initial_merge,
                   merge_factor=self.merge_factor,
                   flip=self.flip,
                   apply_on_selected=self.apply_on_selected)
        send_callback(self)
        return {'FINISHED'}


class LFS_OT_colibri_make_snapshot(bpy.types.Operator):
    '''Make a snapshot (openGlRender) and send it to the server'''
    bl_idname = "lfs.colibri_make_snapshot"
    bl_label = "LFS: Make a snapshot"

    callback_idx: bpy.props.StringProperty()
    callback_uid: bpy.props.StringProperty()
    post_url: bpy.props.StringProperty()

    def get_context_dict(self, context):
        # Find 3D View
        area_view_3d = None
        try:
            area_index = context.window_manager.colibri_active_view
            area_view_3d = context.screen.areas[area_index]
            if area_view_3d.type != "VIEW_3D":
                # Active area is not a 3D view. Layout must have changed.
                # Whatever, use first 3D view in layout.
                area_view_3d = None
                for area in context.screen.areas:
                    if area.type == 'VIEW_3D':
                        area_view_3d = area
                        break
        except:
            pass

        context = context.copy()
        context['area'] = area_view_3d
        context['region'] = area_view_3d.regions[4]
        context['region_data'] = area_view_3d.spaces[0].region_3d
        return context

    def execute(self, context):
        # Create temp file
        with tempfile.TemporaryDirectory() as d:
            path = os.path.join(d, "image.jpeg")

            # Set render settings
            settings = {'bpy.context.scene.render.filepath': path,
                      'bpy.context.scene.render.resolution_x': 300,
                      'bpy.context.scene.render.resolution_y': 300,
                      'bpy.context.scene.render.resolution_percentage': 100,
                      'bpy.context.scene.render.image_settings.color_mode': 'RGB',
                      'bpy.context.scene.render.image_settings.file_format': 'JPEG',}
            settings_orig = {}
            # Save the original settings and apply the thumbnail ones
            for v in settings:
                settings_orig[v] = eval(v)
                exec("%s = %s" % (v, '"%s"' % settings[v] if type(settings[v]) == str else str(settings[v])))


            # Render openGL and write the render
            context = self.get_context_dict(context)
            if context['area'] is None:
                self.report({'ERROR'}, "No 3D View found")
                return {'CANCELLED'}

            bpy.ops.render.opengl(context, write_still=True)

            # Restore previous settings
            for v in settings_orig:
                exec("%s = %s" % (v, '"%s"' % settings_orig[v] if type(settings_orig[v]) == str else str(
settings_orig[v])))

            # Open the rendered image and encode it as base64
            with open(path, "rb") as image_file:
                encoded_image = base64.b64encode(image_file.read()).decode()

            # Callback to warn the image is uploaded
            send_callback(self, {"content": "data:image/jpeg;base64,%s" % encoded_image})
        return {'FINISHED'}


class LFS_OT_colibri_get_pose(bpy.types.Operator):
    '''Get a pose a a base64 encoded json and apply it '''

    bl_idname = "lfs.colibri_get_pose"
    bl_label = "LFS: Get pose"

    callback_idx: bpy.props.StringProperty()
    callback_uid: bpy.props.StringProperty()

    def execute(self, context):
        p = json.dumps(get_pose())
        send_callback(self, {'pose_b64': base64.b64encode(p.encode('ascii')).decode()})
        return {'FINISHED'}


def get_area_index(context, area):
    '''Get specific area's index in the current screen's areas list'''
    for i, ar in enumerate(context.screen.areas):
        if area == ar:
            return i

class LFS_OT_colibri_activate_view(bpy.types.Operator):
    '''Choose a View3D as active. This View will then be used for
    the OpenGL screenshot
    '''
    bl_idname = "lfs.colibri_activate_view"
    bl_label = "Activate 3D View for thumbnail"

    def execute(self, context):
        area_index = get_area_index(context, context.area)
        context.window_manager.colibri_active_view = area_index

        # Force refresh view
        for area in context.screen.areas:
            area.tag_redraw()
        return {'FINISHED'}


def colibri_panel_poselib_menu(self, context):
    layout = self.layout

    area_index = get_area_index(context, context.area)
    is_active_area = area_index == context.window_manager.colibri_active_view

    col = layout.column(align=True)
    row = col.row()
    row.active = not is_active_area
    row.operator("lfs.colibri_activate_view")

    col.label(text="This is the active view" if is_active_area else "",
              icon="INFO" if is_active_area else "")

CLASSES = [
    LFS_OT_colibri_apply_pose,
    LFS_OT_colibri_get_pose,
    LFS_OT_colibri_make_snapshot,
    LFS_OT_colibri_activate_view,
]


def register():
    for cl in CLASSES:
        bpy.utils.register_class(cl)
    bpy.types.WindowManager.colibri_active_view = bpy.props.IntProperty(default=0, min=0, name="Active View")
    bpy.types.LFS_PT_webserver_panel.append(colibri_panel_poselib_menu)

def unregister():
    for cl in reversed(CLASSES):
        bpy.utils.unregister_class(cl)
    del bpy.types.WindowManager.colibri_active_view
    bpy.types.LFS_PT_webserver_panel.remove(colibri_panel_poselib_menu)

if __name__ == "__main__":
    register()
