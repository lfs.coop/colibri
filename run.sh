#!/bin/sh

PORT=${COLIBRI_SERVICE_PORT:-10010}

display_usage() {
  echo ""
  echo "usage :"
  echo "$0 [--port PORT]"
  echo ""
  echo "--port: specific port to run the app on (default: 10010)"
  echo ""
}

for key in "$@"
do

case $key in
  --help|-h)
  echo help
  display_usage
  exit 0
  shift
  ;;
  --port)
  PORT="$2"
  shift
  ;;
  *)    # unknown option
  shift
  ;;
esac
done

# create network if need be
NETWORK_NAME=colibri
if [ ! "$(docker network ls | grep -w ${NETWORK_NAME})" ]; then
  echo "Creating network: ${NETWORK_NAME}!"
  docker network create ${NETWORK_NAME}
else
  echo "${NETWORK_NAME} network already exists!"
fi

# run image
APP_CONTAINER_NAME=colibri-app
if [ "$(docker ps -aq -f name=^${APP_CONTAINER_NAME}$)" ]; then
  docker rm -f $(docker ps -aq -f name=^${APP_CONTAINER_NAME}$)
fi

docker run \
  -d \
  --name ${APP_CONTAINER_NAME} \
  --net ${NETWORK_NAME} \
  -e "EXPRESS_SESSION_SECRET=${EXPRESS_SESSION_SECRET}" \
  -e "KEYCLOAK_ADMIN_URL=${KEYCLOAK_ADMIN_URL}" \
  -e "KEYCLOAK_CLIENT_SECRET=${KEYCLOAK_CLIENT_SECRET}" \
  -e "KEYCLOAK_CLIENT_UUID=${KEYCLOAK_CLIENT_UUID}" \
  -e "KEYCLOAK_REALM=${KEYCLOAK_REALM}" \
  -v $(pwd)/database:/usr/src/app/database \
  -v $(pwd)/client/static/content:/usr/src/app/client/static/content \
  -p ${PORT}:5500 \
  colibri/app
