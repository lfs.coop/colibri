# Colibri

Colibri is a webapp that communicates with Blender instances to share data between various users.

For now, it focuses on animation and rigging poses: it allows you to share pose libraries.

## Installation

### Clone the repo

First, clone the git repo:

```
git clone git@gitlab.com:MinaPecheux/colibri.git
```

### Setup your Keycloak instance

The Keycloak instance is run through Docker, so you must have Docker installed and the daemon running on your computer.

1. Go to the `keycloak/` subfolder and run: `bash make-and-run.sh`.

  **Disclaimer:** once it's finished loading everything and it starts the actual Keycloak setup, the script will first need to connect to the instance. The connection might not be available immediately (because of ongoing startup processes in the newly created container), so there will several attempts - up to 5. Don't stop the script if you see that the connection has failed: just wait for the next retry :)
   
  This script will pull the necessary `jboss/keycloak` and `mysql` images, and it will then run two containers for each to give you an all-setup Keycloak instance.
  
  The created Keycloak instance will have some basic info already created in it:

  - a custom realm `colibri`
  - a custom realm role `colibri`
  - a custom client `colibri`, with an `admin` client role
  - a super-user `default` (password: `default`)
  - a custom user `colibri` (password: `colibri`) with the `colibri` realm-role and the `admin` client-role from the `colibri` client

2. You need to get some information from your local Keycloak interface:

  - go to: `http://localhost:<your-keycloak-port>` (where `<your-keycloak-port>` is the port you exposed Keycloak's `8080` port to; eg http://localhost:8080 by default)
  - click on "Administration Console" in the left block and login with the `default`/`default` user
  - in the left sidebar menu, click on "Clients", then in the table select the `colibri` client
  - get the client UUID from the URL: it is the UUID4 part of the current URL
  - get the client secret by going to the "Credentials" tab and copying the "Secret" text field value

3. Set those two variables in your `.env` file (see the `.env-example` for the name of the keys).

  ⚠️ For the `KEYCLOAK_ADMIN_URL` variable, be sure to use your computer's IP address as the hostname, and not `localhost`!

4. Use this env file in your current shell by running: `source .env`.

### Finish app setup

To wrap things up:

- Go to the root of the repo
- Install the Node packages: `yarn install`
- (For now: link the `colibri-sockets` friend lib with: `yarn link colibri-sockets` - you must have run `tsc` and `yarn link` first in your local `colibri-sockets` lib folder)
- Create the required static content directory (even if it's empty): `mkdir -p client/static/content/poses`
- Start the app in dev mode: `yarn dev`

## Database

### Defining a new table

A complete description of the current database tables is available [here](./docs/database-schemas.md).

To add a new table `<my-table>` to the database:

1. Add a new JavaScript file in `server/helpers/db-schemas` named `<my-table>.js`. If this table belongs to a specific data group (e.g. "anim-rig", put it in a subfolder named after this group).
2. Set up the table definition in this file:

   - create a variable `const TABLE_NAME = <my-table>`
   - define the table schema as an array of objects, where each object is a column, and has the following keys:
     - `name`: the name of the column
     - `type`: the type of the column among Sqlite types: `TEXT`, `INTEGER`, `FLOAT`, `BLOB`
     - `attributes`: array of strings defining specific properties of the column among: `NOT NULL`, `UNIQUE`, `AUTOINCREMENT`, `PRIMARY KEY` (can be an empty array)
     - (optional) `default`: a default value to setup for a new record if the value for this column is not specified
     - (optional) `values`: a list of possible values (for value validation when creating a new record)
     - (optional) `range`: for an `INTEGER` or `REAL` column, object with either one or both `min` and `max` keys to specify/validate that the value must be within this range

     **Warning:** an `id` column (that is an `INTEGER AUTOINCREMENT PRIMARY KEY`) is automatically added to your schema, so you don't need to define it!

     Take a look at the other DB schema definitions file for some examples.

   - finally, export both variables in the `module.exports`

3. Add this table to the `DB_SCHEMAS` variable in `server/helpers/sqlite.js` (top of the file):

   - import your newly created file
   - add the (key, value) pair in the `DB_SCHEMAS`

## Gitlab CI

The `.gitlab-ci.yml` uses 2 environment variables that are predefined by Gitlab (see: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html):

- `CI_DEPLOY_USER`: the username of the Gitlab deploy token to use for Docker image build
- `CI_DEPLOY_PASSWORD`: the password of the Gitlab deploy token to use for Docker image build

This CI needs you to create a **deploy token** on Gitlab (see: https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html#gitlab-deploy-token). The full documentation on this topic is available [over here](https://docs.gitlab.com/ee/user/project/deploy_tokens/). Here is the short version:

1. go to "Settings", then "Repository"
2. scroll down to the "Deploy tokens" block and expand it
3. now, create a new deploy token:
   - name it: `gitlab-deploy-token`
   - give it the following scopes:
     - `read_repository`
     - `read_registry`
     - `write_registry`

  You can keep the default username that is auto-generated from the name you gave your deploy token.

## License

Copyright (C) 2020-2021 M. Pêcheux, Les Fées Spéciales

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program. If not, see
<https://www.gnu.org/licenses/>.
