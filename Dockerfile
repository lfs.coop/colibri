FROM node:14-slim

RUN apt-get update && apt-get install -y g++ build-essential python sqlite3

RUN mkdir -p /usr/src/app
RUN mkdir -p /usr/src/app/client
RUN mkdir -p /usr/src/app/server
RUN mkdir -p /usr/src/app/database

WORKDIR /usr/src/app

COPY ./package.json /usr/src/app/
COPY ./yarn.lock /usr/src/app/yarn.lock

RUN yarn install && yarn cache clean

COPY ./node.config.js /usr/src/app/node.config.js
COPY ./nuxt.config.js /usr/src/app/nuxt.config.js
COPY ./client /usr/src/app/client
RUN yarn build

# COPY ./.env /usr/src/app/.env
COPY ./bin /usr/src/app/bin
COPY ./server /usr/src/app/server

COPY ./start.sh /usr/src/app/start.sh
RUN chmod +x /usr/src/app/start.sh

ENV PORT 5500

EXPOSE ${PORT}

ENV NODE_ENV production

CMD ["sh", "/usr/src/app/start.sh"]
