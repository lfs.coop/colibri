#!/bin/bash

FILE=../database/colibri.sqlite
if [ -f "$FILE" ]; then
  echo "$FILE exists - applying migration"
  sqlite3 $FILE "ALTER TABLE anim_rig_poses \
                 ADD COLUMN approved NOT NULL DEFAULT 0;"

  echo -e "\nMigration applied successfully! :)"
else
  echo "$FILE does not exist."
fi
