const path = require('path');
const multer = require('multer');

const config = require('../../node.config.js');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // Uploads is the Upload_folder_name
    cb(null, path.join(
      config.FRONT_PATH,
      config.CONTENT_FOLDER,
      'uploads',
    ));
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})

const upload = multer({ 
  storage: storage,
  fileFilter: function (req, file, cb) {
    // Set the filetypes, it is optional
    const filetypes = /mp4|webm/;
    const mimetype = filetypes.test(file.mimetype);

    const extname = filetypes.test(
      path.extname(file.originalname).toLowerCase()
    );
    if (mimetype && extname) {
      return cb(null, true);
    }
    cb('Error: File upload only supports the '
      + 'following filetypes: ' + filetypes);
  } 
}).single('file');

module.exports = {
  upload,
};
