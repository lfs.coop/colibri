const AnimRigLibrary = require('../api/models/anim-rig/libraries.js');
const AnimRigPose = require('../api/models/anim-rig/poses.js');

async function checkLibIsMineOrPublicOrIAmAuthorized(req, res, next) {
  const { user } = req;
  if (user.roles.includes(`admin`)) {
    next();
  } else {
    const { name } = req.params;
    const lib = await AnimRigLibrary.findOne({ where: { name } });
    if (lib.owner === user.username || lib.public) {
      next();
    } else {
      res.status(403).send();
    }
  }
}

async function checkLibIsMineOrIAmAuthorized(req, res, next) {
  const { user } = req;
  if (user.roles.includes(`admin`) || user.roles.includes(`anim-lead`)) {
    next();
  } else {
    const { name } = req.params;
    const lib = await AnimRigLibrary.findOne({ where: { name } });
    if (lib.owner === user.username) {
      next();
    } else {
      res.status(403).send();
    }
  }
}

async function checkPoseIsMineOrIAmAuthorized(req, res, next) {
  const { user } = req;
  if (user.roles.includes(`admin`) || user.roles.includes(`anim-lead`)) {
    next();
  } else {
    const { id } = req.params;
    const pose = await AnimRigPose.findOne({ where: { id } });
    const lib = await AnimRigLibrary.findOne({ where: { id: pose.parent_lib } });
    if (lib.owner === user.username) {
      next();
    } else {
      res.status(403).send();
    }
  }
}

module.exports = {
  checkLibIsMineOrPublicOrIAmAuthorized,
  checkLibIsMineOrIAmAuthorized,
  checkPoseIsMineOrIAmAuthorized,
}
