const { sequelize } = require('../api/services/database.js');

async function setupDatabase() {
  // sync all tables
  // > foreach table:
  //    - get the table if it already exists
  //    - else create it
  await sequelize.sync();
}

module.exports = {
  setupDatabase
};
