const queryString = require('querystring');
const axios = require('axios');

const logger = require('colibri-sockets').defaultLogger;

const config = require('../../node.config.js');

const TOKEN_DURATION = 7 * 24 * 60 * 60 * 1000;

class KeycloakClient {

  constructor () {
    // prepare instance properties
    this.baseUrl = config.KEYCLOAK_ADMIN_URL;
    this.realm = config.KEYCLOAK_REALM;
    this.clientId = config.KEYCLOAK_CLIENT_ID;
    this.clientSecret = config.KEYCLOAK_CLIENT_SECRET;
    const base = `${this.baseUrl}/auth/realms/${this.realm}`;
    const baseAdmin = `${this.baseUrl}/auth/admin/realms/${this.realm}`;

    this.accessToken = null;
    this.tokenExpiresAt = new Date();

    this.routes = {
      base,
      baseAdmin,
      token: `${base}/protocol/openid-connect/token`,
      account: `${baseAdmin}/account`,
      users: `${baseAdmin}/users`,
      userRoles: (id) => `${baseAdmin}/users/${id}/role-mappings/clients/${config.KEYCLOAK_CLIENT_UUID}`,
    };
  }

  async getClientToken() {
    const now = new Date();
    const isTokenExpired = (now.valueOf() - this.tokenExpiresAt.valueOf()) >= TOKEN_DURATION;
    if (this.accessToken && !isTokenExpired) return this.accessToken;
    this.tokenExpiresAt = now;
    const config = {
      headers: {
        'Accept': `application/json`,
        'Accept-Charset': `utf-8`,
        'Content-Type': `application/x-www-form-urlencoded`,
      },
    };
    const infos = {
      grant_type: `client_credentials`,
      client_id: this.clientId,
      client_secret: this.clientSecret,
    };
    // we need to stringify the data for x-www-form-urlencoded content type
    const data = queryString.stringify(infos);

    try {
      const response = await axios.post(this.routes.token, data, config);
      const token = response.data.access_token;
      this.accessToken = token;
      logger.success(`[KEYCLOAK] Got token`);
      return token;
    } catch (err) {
      logger.error(err);
      throw (err);
    }
  }

  async wrapAxiosConfig(axiosConfig) {
    const token = await this.getClientToken();
    axiosConfig.method = axiosConfig.method || `get`;
    axiosConfig.headers = axiosConfig.headers || {};
    axiosConfig.headers.Authorization = `bearer ${token}`;
    return axios(axiosConfig);
  }

  async listUsers() {
    try {
      const req = await this.wrapAxiosConfig({ url: this.routes.users });
      return (await req).data;
    } catch (err) {
      logger.error(err);
      throw (err);
    }
  }

  async getUserRoles(userId) {
    try {
      const req = await this.wrapAxiosConfig({ url: this.routes.userRoles(userId) });
      return (await req).data;
    } catch (err) {
      logger.error(err);
      throw (err);
    }
  }

}

module.exports = {
  KeycloakClient,
};
