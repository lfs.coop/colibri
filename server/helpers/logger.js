const morgan = require('morgan');
const chalk = require('chalk');
const consola = require('consola');

const config = require('../../node.config.js');

consola.level = config.LOGGER_LEVEL;

const COLOR_CODES = {
  0: `yellow`,
  1: `green`,
  2: `green`,
  3: `cyan`,
  4: `yellow`,
  5: `red`,
  7: `magenta`,
};

const logger = consola.create({
  reporters: [ new consola[ config.isDev ? `FancyReporter` : `JSONReporter` ]() ]
});

const logRequest = (tokens, req, res) => {
  const method = chalk.cyan(tokens.method(req, res));
  const url = chalk.white(tokens.url(req, res));
  return `${chalk.grey('==> ')} [${method}] ${url}`;
}

const logResponse = (tokens, req, res) => {
  const method = chalk.cyan(tokens.method(req, res));
  const url = chalk.white(tokens.url(req, res));
  const status = tokens.status(req, res);
  const responseTime = `${tokens[`response-time`](req, res)}ms`;
  // eslint no-bitwise: "off"
  const s = (status / 100) | 0;
  const statusColor = Object.prototype.hasOwnProperty.call(COLOR_CODES, s) ? COLOR_CODES[s] : `grey`;
  return `${chalk.grey(`<== `)} [${method}] ${url} ${chalk[statusColor](status)} ${chalk.grey(responseTime)}`;
}

logger.logRequest = () => morgan(logRequest, { immediate: true });
logger.logResponse = () => morgan(logResponse);

module.exports = logger;
