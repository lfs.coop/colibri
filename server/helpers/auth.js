`use strict`;

const _ = require('lodash');
const ExpressSession = require('express-session');
const Keycloak = require('keycloak-connect');
const jwt = require('jsonwebtoken');

const config = require('../../node.config.js');
const { KeycloakClient } = require('./users.js');

const memoryStore = new ExpressSession.MemoryStore();

const keycloakSession = new ExpressSession({
  secret: config.EXPRESS_SESSION_SECRET,
  resave: false,
  saveUninitialized: true,
  store: memoryStore,
  cookie: { secure: config.EXPRESS_COOKIE_SECURE, path: config.ROUTER_BASE },
});

const kcConfig = {
  'clientId': config.KEYCLOAK_CLIENT_ID,
  'serverUrl': `${config.KEYCLOAK_ADMIN_URL}/auth`,
  'ssl-required': `external`,
  'realm': config.KEYCLOAK_REALM,
  'credentials': {
    secret: config.KEYCLOAK_CLIENT_SECRET,
  },
};

const keycloak = new Keycloak({ store: memoryStore }, kcConfig);
const keycloakAdminCli = new KeycloakClient();

function userInfo(req, res, next) {
  const token = JSON.parse(req.kauth.grant.__raw).access_token;
  const tokenDecode = jwt.decode(token);

  const roles = _.get(tokenDecode, [`resource_access`, config.KEYCLOAK_CLIENT_ID, `roles`], []);

  const user = {
    id: tokenDecode.sub,
    username: tokenDecode.preferred_username,
    email: tokenDecode.email,
    access_token: token,
    roles,
  };
  req.user = user;
  return next();
}

module.exports = {
  session: keycloakSession,
  keycloak,
  keycloakAdminCli,
  userInfo,
};
