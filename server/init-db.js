const sqlite = require('./helpers/sqlite.js');

const dropTables = true;
const addSamples = false;
sqlite.initializeDatabase(dropTables, addSamples);
