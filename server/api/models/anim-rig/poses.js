const { DataTypes } = require('sequelize');
const { sequelize } = require('../../services/database.js');
const AnimRigLibrary = require('./libraries.js');

const AnimRigPose = sequelize.define('anim_rig_poses', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  parent_lib: {
    type: DataTypes.INTEGER,
    references: {
      model: AnimRigLibrary,
      key: 'id'
    }
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  data: {
    type: DataTypes.STRING,
    allowNull: false,
    get: function() {
      const val = this.getDataValue('data');
      if (!val) return null;
      if (typeof val === 'string') return JSON.parse(val);
      else return val;
    }, 
    set: function(val) {
      return this.setDataValue('data', JSON.stringify(val));
    },
    defaultValue: '{}'
  },
  dataLength: {
    type: DataTypes.VIRTUAL,
    defaultValue: 0,
  },
  type: {
    type: DataTypes.ENUM,
    allowNull: false,
    values: ['pose', 'selection set'],
    defaultValue: 'pose'
  },
  thumbnail: {
    type: DataTypes.STRING
  },
  thumbnail_movie: {
    type: DataTypes.STRING
  },
  use_count: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false
  },
  is_animated: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false
  },
  active: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: true
  },
  approved: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false
  }
}, {
  freezeTableName: true
});

module.exports = AnimRigPose;
