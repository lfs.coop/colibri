const { Model, DataTypes } = require('sequelize');
const { sequelize } = require('../../services/database.js');
const Project = require('../projects.js');

// (declare model first to allow for self-referencing field)
class AnimRigLibrary extends Model {}

AnimRigLibrary.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  parent_project: {
    type: DataTypes.INTEGER,
    references: {
      model: Project,
      key: 'id'
    }
  },
  parent_lib: {
    type: DataTypes.INTEGER,
    references: {
      model: AnimRigLibrary,
      key: 'id'
    }
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  owner: {
    type: DataTypes.STRING,
    allowNull: false
  },
  favorite_for_users: {
    type: DataTypes.TEXT,
    allowNull: false,
    get: function() {
      const val = this.getDataValue('favorite_for_users');
      if (val === '') return [];
      return val.split(';');
    }, 
    set: function(val) {
      return this.setDataValue('favorite_for_users', val.join(';'));
    },
    defaultValue: ''
  },
  public: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false
  },
  active: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: true
  }
}, {
  sequelize,
  modelName: 'anim_rig_libraries'
});

module.exports = AnimRigLibrary;
