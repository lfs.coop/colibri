const { DataTypes } = require('sequelize');
const { sequelize } = require('../services/database.js');

const Project = sequelize.define('projects', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  readers: {
    type: DataTypes.TEXT,
    allowNull: false,
    get: function() {
      const val = this.getDataValue('readers');
      if (val === '') return [];
      return val.split(';');
    }, 
    set: function(val) {
      return this.setDataValue('readers', val.join(';'));
    },
    defaultValue: ''
  },
  writers: {
    type: DataTypes.TEXT,
    allowNull: false,
    get: function() {
      const val = this.getDataValue('writers');
      if (val === '') return [];
      return val.split(';');
    }, 
    set: function(val) {
      return this.setDataValue('writers', val.join(';'));
    },
    defaultValue: ''
  },
  active: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: true
  }
}, {
  freezeTableName: true
});

module.exports = Project;
