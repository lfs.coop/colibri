const { DataTypes } = require('sequelize');
const { sequelize } = require('../services/database.js');

const Websocket = sequelize.define('websockets', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  host: {
    type: DataTypes.STRING,
    allowNull: false
  },
  port: {
    type: DataTypes.INTEGER,
    allowNull: false,
    validate: {
      min: 6000
    }
  }
}, {
  freezeTableName: true
});

module.exports = Websocket;
