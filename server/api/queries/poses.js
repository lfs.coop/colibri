const AnimRigLibrary = require('../models/anim-rig/libraries.js');
const AnimRigPose = require('../models/anim-rig/poses.js');

/* CREATE */
async function createPose({ lib, libId, data }) {
  if (libId) {
    return AnimRigPose.create({ ...data, parent_lib: libId });
  } else {
    const libRef = await AnimRigLibrary.findOne({ where: { name: lib } });
    return AnimRigPose.create({ ...data, parent_lib: libRef.id });
  }
}

/* READ */
async function getPose(query) {
  return AnimRigPose.findOne({ where: query });
}

/* UPDATE */
async function updatePose(query, update) {
  // get initial pose data
  const pose = await AnimRigPose.findOne({ where: query });
  // update and save
  for (const key of Object.keys(update)) {
    pose[key] = update[key];
  }
  await pose.save();
  return pose;
}

/* DELETE */
async function deletePose(query) {
  return AnimRigPose.update({ active: false }, { where: query });
}

module.exports = {
  // CREATE
  createPose,
  // READ
  getPose,
  // UPDATE
  updatePose,
  // DELETE
  deletePose,
};
