const { Op } = require("sequelize");
const AnimRigLibrary = require('../models/anim-rig/libraries.js');
const AnimRigPose = require('../models/anim-rig/poses.js');

async function injectDefaultLibraryData(libData) {
  libData.poses = [];
  libData.favorite = false;
  libData.parent_lib = null;
  return libData;
}

async function injectAllLibraryData(libData, user, options = {}) {
  const getInactivePoses = 'getInactivePoses' in options ? options.getInactivePoses : false;
  const withPoses = 'withPoses' in options ? options.withPoses : true;
  const noPoseData = 'noPoseData' in options ? options.noPoseData : false;
  if (withPoses) {
    const poses = await getLibraryPoses({ libData, getInactivePoses });
    if (noPoseData) poses.forEach((p) => p.data = {});
    libData.poses = poses;
  }
  libData.favorite = libData.favorite_for_users.includes(user.username);
  return libData;
}

/* CREATE */
async function createLibrary({ data, user }) {
  const lib = AnimRigLibrary.build({ ...data, owner: user.username });
  await lib.save();
  return injectDefaultLibraryData(lib.get());
}

/* READ */
async function getLibraryPoses({ libData, getInactivePoses }) {
  const query = { parent_lib: libData.id };
  if (!getInactivePoses) {
    query.active = true;
  }
  return AnimRigPose.findAll({ where: query })
    .then((poses) => {
      poses.forEach((p) => { p.dataLength = Object.keys(p.data).length; });
      return poses;
    });
}

async function getLibrary(user, query, options) {
  const lib = await AnimRigLibrary.findOne({ where: query });
  return injectAllLibraryData(lib.get(), user, options);
}

async function listLibraries(user, queryData, options) {
  const { public, excludeUserLibs } = queryData;
  const { getPoses, getInactiveLibs } = options;
  let libsPromise;
  if (Object.keys(queryData).length > 0) {
    const query = { public };
    const { username } = user;
    if (!getInactiveLibs) {
      query.active = true;
    }
    if (!public && user) {
      query.owner = username;
    } else if (excludeUserLibs) {
      query.owner = { [Op.ne]: username };
    }
    libsPromise = AnimRigLibrary.findAll({ where: query });
  } else {
    libsPromise = AnimRigLibrary.findAll();
  }
  const libs = await Promise.all(
    (await libsPromise).map(async lib => injectAllLibraryData(
      lib.get(),
      user,
      { withPoses: getPoses, getInactivePoses: false }
    ))
  );
  return libs;
}

/* UPDATE */
async function updateLibrary(user, query, update) {
  // get initial lib data
  const lib = await AnimRigLibrary.findOne({ where: query });
  // update and save
  for (const key of Object.keys(update)) {
    lib[key] = update[key];
  }
  await lib.save();
  // return complete object
  return injectAllLibraryData(lib.get(), user);
}

async function updateLibraryFavorite(user, query, favorite, options) {
  const { username } = user;
  // get initial lib data
  const lib = await AnimRigLibrary.findOne({ where: query, plain: true });
  if (favorite && !lib.favorite_for_users.includes(username)) {
    lib.favorite_for_users = [...lib.favorite_for_users, username];
  } else if (!favorite && lib.favorite_for_users.includes(username)) {
    lib.favorite_for_users = lib.favorite_for_users.filter(u => u !== username);
  }
  await lib.save();
  return injectAllLibraryData(lib.get(), user, { withPoses: options.getPoses });
}

/* DELETE */
async function deleteLibrary(query) {
  return AnimRigLibrary.update({ active: false }, { where: query });
}

module.exports = {
  // CREATE
  createLibrary,
  // READ
  getLibrary,
  listLibraries,
  // UPDATE
  updateLibrary,
  updateLibraryFavorite,
  // DELETE
  deleteLibrary,
  // UTILS,
  injectAllLibraryData
};
