const Websocket = require('../models/websockets.js');

/* CREATE */
async function createWebsocket({ data }) {
  return Websocket.create(data);
}

/* READ */
async function getWebsocket(query) {
  return Websocket.findOne({ where: query });
}

async function listWebsockets() {
  return Websocket.findAll();
}

/* DELETE */
async function deleteWebsocket({ id }) {
  return Websocket.destroy({ where: { id } });
}

module.exports = {
  // CREATE
  createWebsocket,
  // READ
  getWebsocket,
  listWebsockets,
  // DELETE
  deleteWebsocket,
};
