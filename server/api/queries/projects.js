const Project = require('../models/projects.js');
const AnimRigLibrary = require('../models/anim-rig/libraries.js');
const { injectAllLibraryData } = require('./libraries.js');

async function injectDefaultProjectData(projectData) {
  projectData.animRigLibraries = [];
  return projectData;
}

async function injectAllProjectData(projectData, options = {}) {
  const user = options.user;
  const getAnimRigLibraries = 'getAnimRigLibraries' in options ? options.getAnimRigLibraries : false;
  if (getAnimRigLibraries) {
    const libsPromise = await AnimRigLibrary.findAll({ where: { parent_project: projectData.id } });
    let libsData;
    if (user) {
      libsData = await Promise.all(
        (await libsPromise).map(async lib => injectAllLibraryData(
          lib.get(),
          user,
          { withPoses: true, noPoseData: true, getInactivePoses: true }
        ))
      );
    } else {
      libsData = await libsPromise;
    }
    projectData.animRigLibraries = libsData;
  }
  return projectData;
}

/* CREATE */
async function createProject({ data }) {
  const project = Project.build(data);
  await project.save();
  return injectDefaultProjectData(project.get());
}

/* READ */
async function getProject(query, options) {
  const project = await Project.findOne({ where: query });
  if (project === null) return null;
  return injectAllProjectData(project.get(), options);
}

async function listProjects(options = {}) {
  const pr = await Project.findAll();
  const projects = await Promise.all(
    pr.map(async project => injectAllProjectData(project.get(), options))
  );
  return projects;
}

/* UPDATE */
async function updateProject(query, update, options = {}) {
  // update
  await Project.update(update, { where: query });
  // return complete object
  return injectAllProjectData((await Project.findOne({ where: query })).get(), options);
}

async function updateProjectAnimRigLibraries(query, librariesToAddIds, librariesToRemoveIds, options = {}) {
  // get initial project data
  const project = await Project.findOne({ where: query });
  // assign project id as parent for libraries, or reset to null
  await AnimRigLibrary.update({ parent_project: project.id }, { where: { id: librariesToAddIds } });
  await AnimRigLibrary.update({ parent_project: null }, { where: { id: librariesToRemoveIds } });
  // return complete object
  return injectAllProjectData(project.get(), options);
}

/* DELETE */
async function deleteProject({ id }) {
  // set the project inactive
  return Project.update({ active: 0 }, { where: { id } });
}

module.exports = {
  // CREATE
  createProject,
  // READ
  getProject,
  listProjects,
  // UPDATE
  updateProject,
  updateProjectAnimRigLibraries,
  // DELETE
  deleteProject,
};
