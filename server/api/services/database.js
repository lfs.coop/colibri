const path = require('path');
const { Sequelize } = require('sequelize');
const config = require("../../../node.config.js");

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: path.join(config.ROOT_PATH, config.SQLITE_DB_FOLDER, config.SQLITE_DB),
  // pool configuration used to pool database connections
  pool: {
    max: config.SQLITE_DB_MAX_POOL,
    idle: 30000,
    acquire: 60000,
  },
  logging: process.env.NODE_ENV === 'production' ? false : console.log
});

async function connect() {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

module.exports = {
  connect,
  sequelize
};
