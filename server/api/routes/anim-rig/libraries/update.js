const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const asyncHandler = require('express-async-handler');

const config = require('../../../../../node.config.js');
const librariesQueries = require('../../../queries/libraries.js');

async function updateLibrary(req, res) {
  const { group } = req.query;
  const { name } = req.params;
  const renaming = _.has(req.body, `name`);
  const newName = req.body.name;
  const query = {
    public: {
      name, public: true
    },
    user: {
      name, owner: req.user.username
    }
  }[group] || null;
  if (query === null) {
    res.status(500).send(`sqlite-set-error`);
  }
  let lib;
  try {
    lib = await librariesQueries.updateLibrary(req.user, query, req.body);
  } catch (error) {
    res.status(500).send(`sqlite-set-error`);
  }
  // if changing lib name: rename poses content dir if one exists
  if (renaming) {
    const libContentFolder = path.join(
      config.FRONT_PATH,
      config.CONTENT_FOLDER,
      `poses`,
      name
    );
    if (fs.existsSync(libContentFolder)) {
      fs.renameSync(libContentFolder, path.join(
        config.FRONT_PATH,
        config.CONTENT_FOLDER,
        `poses`,
        newName
      ));
    }
  }
  res.json(lib);
}

async function updateLibraryFavorite(req, res) {
  const getPoses = req.query.getPoses === `true`;
  try {
    const lib = await librariesQueries.updateLibraryFavorite(
      req.user,
      { name: req.params.name },
      req.body.favorite,
      { getPoses },
    );
    res.json(lib);
  } catch (error) {
    console.log(error);
    res.status(500).send(`sqlite-set-error`);
  }
}

module.exports = {
  updateLibrary: asyncHandler(updateLibrary),
  updateLibraryFavorite: asyncHandler(updateLibraryFavorite),
}
