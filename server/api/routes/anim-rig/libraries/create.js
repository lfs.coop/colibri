const asyncHandler = require('express-async-handler');

const librariesQueries = require('../../../queries/libraries.js');

async function createUserLibrary(req, res) {
  try {
    const lib = await librariesQueries.createLibrary({
      data: req.body,
      user: req.user
    });
    if (lib === null) {
      res.status(500).send(`sqlite-set-error`);
    } else {
      res.json(lib);
    }
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  }
}

module.exports = {
  createUserLibrary: asyncHandler(createUserLibrary),
}
