const asyncHandler = require('express-async-handler');

const librariesQueries = require('../../../queries/libraries.js');

async function deleteLibrary(req, res) {
  try {
    await librariesQueries.deleteLibrary({ name: req.params.name });
    res.status(200).send('ok');
  } catch (error) {
    res.status(500).send(`sqlite-delete-error`);
  }
}

module.exports = {
  deleteLibrary: asyncHandler(deleteLibrary),
}
