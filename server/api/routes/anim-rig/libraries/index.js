'use strict';

const express = require('express');

const { checkLibIsMineOrPublicOrIAmAuthorized, checkLibIsMineOrIAmAuthorized } = require('../../../../helpers/middlewares.js');

const create = require('./create.js');
const read = require('./read.js');
const update = require('./update.js');
const _delete = require('./delete.js');

const router = express.Router();

// -- CREATE
router.post(`/`, create.createUserLibrary);

// -- READ
router.get(`/`, read.listLibraries);
router.get(`/:name`, checkLibIsMineOrPublicOrIAmAuthorized, read.getLibrary);

// -- UPDATE
router.put(`/:name`, checkLibIsMineOrIAmAuthorized, update.updateLibrary);
router.put(`/:name/favorite`, update.updateLibraryFavorite);

// -- DELETE
router.delete(`/:name`, checkLibIsMineOrIAmAuthorized, _delete.deleteLibrary);

module.exports = router;
