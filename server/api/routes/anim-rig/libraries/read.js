const asyncHandler = require('express-async-handler');

const librariesQueries = require('../../../queries/libraries.js');

async function getLibrary(req, res) {
  const { name } = req.params;
  const lib = await librariesQueries.getLibrary(
    req.user,
    { name },
    { getInactivePoses: req.query.showInactive === `true` }
  );
  if (lib === null) {
    res.status(500).send(`sqlite-get-error`);
  } else {
    res.json(lib);
  }
}

async function listLibraries(req, res) {
  const { group } = req.query;
  const showInactive = req.query.showInactive === `true`;
  const getPoses = req.query.getPoses === `true`;
  const excludeUserLibs = req.query.excludeUserLibs || false;
  const query = group ? ({
    public: {
      public: true,
      excludeUserLibs
    },
    user: {
      public: false,
      excludeUserLibs: false
    },
    all: {
      public: true,
      excludeUserLibs: false
    }
  }[group] || null) : {};
  if (query === null) {
    res.status(500).send(`sqlite-get-error`);
  }
  let libs;
  try {
    libs = await librariesQueries.listLibraries(
      req.user,
      query,
      { getPoses, getInactiveLibs: showInactive }
    );
  } catch (error) {
    res.status(500).send(`sqlite-get-error`);
  }
  res.json(libs);
}

module.exports = {
  getLibrary: asyncHandler(getLibrary),
  listLibraries: asyncHandler(listLibraries),
}
