const fs = require('fs');
const path = require('path');
const asyncHandler = require('express-async-handler');

const config = require('../../../../../node.config.js');
const librariesQueries = require('../../../queries/libraries.js');
const posesQueries = require('../../../queries/poses.js');
const { upload } = require('../../../../helpers/multer.js');

async function updatePose(req, res) {
  try {
    const pose = await posesQueries.updatePose(
      { id: req.params.id },
      req.body
    );
    res.json(pose);
  } catch (error) {
    res.status(500).send(`sqlite-set-error`);
  }
}

async function updatePoseThumbnail(req, res) {
  const { id } = req.params;

  // get previous thumbnail reference
  const pose = await posesQueries.getPose({ id });
  const lib_id = pose.parent_lib;
  const lib = await librariesQueries.getLibrary(
    req.user,
    { id: lib_id },
    {
      withPoses: false,
      getInactivePoses: req.query.showInactive === `true`
    },
  );
  const poseThumbPath = path.join(
    config.FRONT_PATH,
    config.CONTENT_FOLDER,
    `poses`,
    lib.name
  );
  // make dir if need be
  if (!fs.existsSync(poseThumbPath)) {
    fs.mkdirSync(poseThumbPath, { recursive: true });
  }

  // remove previous thumbnail
  if (pose.thumbnail) {
    const poseThumbFullPath = path.join(poseThumbPath, pose.thumbnail);
    if (fs.existsSync(poseThumbFullPath)) {
      fs.unlinkSync(poseThumbFullPath);
    }
  }

  // store new thumbnail
  const regex = /^data:.+\/(.+);base64,(.*)$/;
  const [_, ext, data] = [ ...req.body.content.match(regex) ];
  const newThumbnailName = `${pose.name}.${ext}`
  fs.writeFileSync(path.join(poseThumbPath, newThumbnailName), Buffer.from(data, `base64`));

  // update thumbnail reference
  const result = await posesQueries.updatePose({ id }, { thumbnail: newThumbnailName });
  res.json(result);
}

async function updatePoseThumbnailMovie(req, res) {
  const uploadsPath = path.join(config.FRONT_PATH, config.CONTENT_FOLDER, 'uploads');
  if (!fs.existsSync(uploadsPath)) {
    fs.mkdirSync(uploadsPath, { recursive: true });
  }
  upload(req, res, async function (err) {
    if (err) {
      console.error(err);
      res.status(500).send(`update-file-error`);
    }
    else {
      const { id } = req.params;
      
      // get previous thumbnail reference
      const pose = await posesQueries.getPose({ id });
      const thumbnailName = req.file.originalname;
      const poseThumbMoviePath = path.join(
        config.FRONT_PATH,
        config.CONTENT_FOLDER,
        `uploads`,
      );
      
      // remove previous thumbnail if name has changed (else file
      // has already been replaced)
      if (pose.thumbnail_movie && pose.thumbnail_movie !== thumbnailName) {
        console.log('removing old file:', pose.thumbnail_movie)
        const poseThumbFullPath = path.join(poseThumbMoviePath, pose.thumbnail_movie);
        if (fs.existsSync(poseThumbFullPath)) {
          fs.unlinkSync(poseThumbFullPath);
        }
      }
      
      // update thumbnail reference
      const result = await posesQueries.updatePose({ id }, { thumbnail_movie: thumbnailName });
      res.json(result);
    }
  })

}

module.exports = {
  updatePose: asyncHandler(updatePose),
  updatePoseThumbnail: asyncHandler(updatePoseThumbnail),
  updatePoseThumbnailMovie: asyncHandler(updatePoseThumbnailMovie),
}
