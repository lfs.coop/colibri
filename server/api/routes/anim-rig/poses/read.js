const asyncHandler = require('express-async-handler');

const posesQueries = require('../../../queries/poses.js');

async function getData(req, res) {
  const { id } = req.params;
  const pose = await posesQueries.getPose({ id });
  if (pose === null) {
    res.status(500).send(`sqlite-get-error`);
  } else {
    res.json(pose.data);
  }
}

module.exports = {
  getData: asyncHandler(getData),
}
