'use strict';

const express = require('express');

const { checkPoseIsMineOrIAmAuthorized } = require('../../../../helpers/middlewares.js');

const read = require('./read.js');
const create = require('./create.js');
const update = require('./update.js');
const _delete = require('./delete.js');

const router = express.Router();

// -- READ
router.get(`/:id/data`, read.getData);

// -- CREATE
router.post(`/`, create.createPose);
router.post(`/:id/copy`, create.copyPoseToLibs);

// -- UPDATE
router.put(`/:id`, checkPoseIsMineOrIAmAuthorized, update.updatePose);
router.put(`/:id/thumbnail`, checkPoseIsMineOrIAmAuthorized, update.updatePoseThumbnail);
router.put(`/:id/thumbnail-movie`, checkPoseIsMineOrIAmAuthorized, update.updatePoseThumbnailMovie);

// -- DELETE
router.delete(`/:id`, checkPoseIsMineOrIAmAuthorized, _delete.deletePose);

module.exports = router;
