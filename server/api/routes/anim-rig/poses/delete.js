const asyncHandler = require('express-async-handler');

const posesQueries = require('../../../queries/poses.js');

async function deletePose(req, res) {
  try {
    await posesQueries.deletePose({ id: req.params.id });
    res.status(200).send('ok');
  } catch (error) {
    res.status(500).send(`sqlite-delete-error`);
  }
}

module.exports = {
  deletePose: asyncHandler(deletePose),
}
