const fs = require('fs');
const path = require('path');
const asyncHandler = require('express-async-handler');

const config = require('../../../../../node.config.js');
const librariesQueries = require('../../../queries/libraries.js');
const posesQueries = require('../../../queries/poses.js');

async function createPose(req, res) {
  const { lib, ...data } = req.body;
  try {
    const pose = await posesQueries.createPose({ lib, data });
    res.json(pose);
  } catch (error) {
    console.log(error)
    let msg = `sqlite-set-error`;
    if (error.name === 'SequelizeUniqueConstraintError') {
      msg = `already-exists`;
    }
    res.status(500).send(msg);
  }
}

async function copyPoseToLibs(req, res) {
  const { poseData, libIds } = req.body;
  delete poseData.id;
  const originalLib = await librariesQueries.getLibrary(
    req.user,
    { id: poseData.parent_lib },
    { withPoses: false, getInactivePoses: false },
  );
  let originalPoseThumbPath, thumbExt;
  if (poseData.thumbnail) {
    originalPoseThumbPath = path.join(
      config.FRONT_PATH,
      config.CONTENT_FOLDER,
      `poses`,
      originalLib.name,
      poseData.thumbnail
    );
    const tmp = poseData.thumbnail.split(`.`);
    thumbExt = tmp[tmp.length - 1];
  } else {
    originalPoseThumbPath = null;
    thumbExt = null;
  }
  let pose, poseName, poseOffset, creating, newLib, newPoseThumbsPath, newPoseThumbPath;
  const newPoses = [];
  for (const libId of libIds) {
    pose = null;
    poseOffset = 0;
    poseName = `${poseData.name} (${libId} - ${poseOffset})`;
    creating = true;
    while (creating) {
      try {
        pose = await posesQueries.createPose({
          libId,
          data: {
            ...poseData,
            name: poseName,
            thumbnail: originalPoseThumbPath ? `${poseName}.${thumbExt}` : null,
          }
        });
        break;
      } catch (err) {
        if (err.name === 'SequelizeUniqueConstraintError') {
          poseOffset += 1;
          poseName = `${poseData.name} (${libId} - ${poseOffset})`;
          continue;
        } else {
          break;
        }
      }
    }
    if (pose === null) {
      console.error(`Error copying pose in lib ${libId}`);
      continue;
    }
    // copy thumbnail
    if (originalPoseThumbPath) {
      newLib = await librariesQueries.getLibrary(
        req.user,
        { id: parseInt(libId) },
        { withPoses: false, getInactivePoses: false },
      );
      newPoseThumbsPath = path.join(
        config.FRONT_PATH,
        config.CONTENT_FOLDER,
        `poses`,
        newLib.name
      );
      if (!fs.existsSync(newPoseThumbsPath)) {
        fs.mkdirSync(newPoseThumbsPath, { recursive: true });
      }
      newPoseThumbPath = path.join(
        newPoseThumbsPath,
        `${poseName}.${thumbExt}`
      );
      fs.copyFileSync(
        originalPoseThumbPath,
        newPoseThumbPath,
      );
    }
    // store new pose in other lib
    newPoses.push(pose);
  }
  if (newPoses.length > 0) {
    res.json(newPoses);
  } else {
    res.status(500).send('sqlite-create-error');
  }
}

module.exports = {
  createPose: asyncHandler(createPose),
  copyPoseToLibs: asyncHandler(copyPoseToLibs),
}
