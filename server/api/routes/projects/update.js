const asyncHandler = require('express-async-handler');

const projectsQueries = require('../../queries/projects.js');

async function updateProject(req, res) {
  const { id } = req.params;
  const getAnimRigLibraries = 'getAnimRigLibraries' in req.query
    ? req.query.getAnimRigLibraries === `true`
    : true;
  const options = { user: req.user, getAnimRigLibraries };
  let project;
  try {
    project = await projectsQueries.updateProject({ id }, req.body, options);
    res.json(project);
  } catch (error) {
    console.log(error);
    res.status(500).send(`sqlite-set-error`);
  }
}

async function updateProjectAnimRigLibraries(req, res) {
  const { id } = req.params;
  const getAnimRigLibraries = 'getAnimRigLibraries' in req.query
    ? req.query.getAnimRigLibraries === `true`
    : true;
  const options = { getAnimRigLibraries };
  let project;
  try {
    const { toAdd, toRemove } = req.body.libraries;
    project = await projectsQueries.updateProjectAnimRigLibraries({ id }, toAdd, toRemove, options);
    res.json(project);
  } catch (error) {
    res.status(500).send(`sqlite-set-error`);
  }
}

module.exports = {
  update: asyncHandler(updateProject),
  updateAnimRigLibraries: asyncHandler(updateProjectAnimRigLibraries),
}
