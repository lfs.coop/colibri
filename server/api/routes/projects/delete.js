const asyncHandler = require('express-async-handler');

const projectsQueries = require('../../queries/projects.js');

async function _delete(req, res) {
  try {
    await projectsQueries.deleteProject({ id: req.params.id });
    res.status(200).send();
  } catch (err) {
    res.status(500).send(`sqlite-delete-error`);
  }
}

module.exports = {
  delete: asyncHandler(_delete),
}
