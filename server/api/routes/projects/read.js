const asyncHandler = require('express-async-handler');

const projectsQueries = require('../../queries/projects.js');

async function list(req, res) {
  const getAnimRigLibraries = 'getAnimRigLibraries' in req.query
    ? req.query.getAnimRigLibraries === `true`
    : true;
  const options = { user: req.user, getAnimRigLibraries };
  let projects = await projectsQueries.listProjects(options);

  // if not admin: filter projects to only the readable projects
  if (!req.user.roles.includes(`admin`)) {
    projects = projects.filter(p => {
      return (
        p.readers.includes(req.user.username) ||
        p.writers.includes(req.user.username)
      );
    });
  }

  res.json(projects);
}

module.exports = {
  list: asyncHandler(list),
}
