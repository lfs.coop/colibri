const asyncHandler = require('express-async-handler');

const projectsQueries = require('../../queries/projects.js');

async function create(req, res) {
  const getAnimRigLibraries = 'getAnimRigLibraries' in req.query
    ? req.query.getAnimRigLibraries === `true`
    : true;
  const options = { getAnimRigLibraries };
  const data = req.body;
  const alreadyExists = await projectsQueries.getProject(data, options);
  if (alreadyExists === null) {
    try {
      const project = await projectsQueries.createProject({ data });
      res.json(project);
    } catch (error) {
      res.status(500).send(error);
    }
  } else {
    res.status(500).send(`params-already-taken`);
  }
}

module.exports = {
  create: asyncHandler(create),
}
