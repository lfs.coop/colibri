'use strict';

const express = require('express');

const auth = require('../../helpers/auth.js');
const config = require('../../../node.config.js');

const animRigLibrariesRouter = require('./anim-rig/libraries/index.js');
const animRigPosesRouter = require('./anim-rig/poses/index.js');
const projectsRouter = require('./projects/index.js');
const usersRouter = require('./users/index.js');
const websocketsRouter = require('./websockets/index.js');

const apiRouter = express.Router();

// setup keycloak protection
apiRouter.use(auth.keycloak.protect(`realm:${config.KEYCLOAK_ROLE}`));

// -- ANIM/RIG
apiRouter.use(`/anim-rig/libraries`, animRigLibrariesRouter);
apiRouter.use(`/anim-rig/poses`, animRigPosesRouter);

// -- PROJECTS
apiRouter.use(`/projects`, projectsRouter);

// -- USERS
apiRouter.use(`/users`, usersRouter);

// -- WEBSOCKETS
apiRouter.use(`/websockets`, websocketsRouter);

module.exports = apiRouter;
