const asyncHandler = require('express-async-handler');

const { keycloakAdminCli } = require('../../../helpers/auth.js');

async function list(req, res) {
  const users = await keycloakAdminCli.listUsers();
  for (let user of users) {
    user.roles = await keycloakAdminCli.getUserRoles(user.id);
  }
  res.json(users);
}

async function getCurrent(req, res) {
  // (directly use the "user" that was injected by the keycloak middleware)
  res.json(req.user);
}

module.exports = {
  list: asyncHandler(list),
  getCurrent: asyncHandler(getCurrent),
}
