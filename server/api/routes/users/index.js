'use strict';

const express = require('express');

const auth = require('../../../helpers/auth.js');

const read = require('./read.js');

const router = express.Router();

// -- READ
router.get(`/`, auth.keycloak.protect(`admin`), read.list);
router.get(`/current`, read.getCurrent);

module.exports = router;
