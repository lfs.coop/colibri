'use strict';

const express = require('express');

const create = require('./create.js');
const read = require('./read.js');
const _delete = require('./delete.js');

const router = express.Router();

// -- CREATE
router.post(`/`, create.create);

// -- READ
router.get(`/`, read.list);

// -- DELETE
router.delete(`/:id`, _delete.delete);

module.exports = router;
