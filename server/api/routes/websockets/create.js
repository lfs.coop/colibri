const asyncHandler = require('express-async-handler');

const websocketsQueries = require('../../queries/websockets.js');

async function create(req, res) {
  const data = req.body;
  const alreadyExists = await websocketsQueries.getWebsocket(data);
  if (alreadyExists === null) {
    try {
      const websocket = await websocketsQueries.createWebsocket({ data });
      res.json(websocket);
    } catch (error) {
      res.status(500).send(error);
    }
  } else {
    res.status(500).send(`params-already-taken`);
  }
}

module.exports = {
  create: asyncHandler(create),
}
