const asyncHandler = require('express-async-handler');

const websocketsQueries = require('../../queries/websockets.js');

async function _delete(req, res) {
  try {
    await websocketsQueries.deleteWebsocket({ id: req.params.id });
    res.status(200).send();
  } catch (err) {
    res.status(500).send(`sqlite-delete-error`);
  }
}

module.exports = {
  delete: asyncHandler(_delete),
}
