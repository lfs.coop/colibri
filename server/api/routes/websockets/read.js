const asyncHandler = require('express-async-handler');

const websocketsQueries = require('../../queries/websockets.js');

async function list(req, res) {
  const websockets = await websocketsQueries.listWebsockets();
  res.json(websockets);
}

module.exports = {
  list: asyncHandler(list),
}
