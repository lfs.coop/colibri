const http = require('http');
const bodyParser = require('body-parser');
const express = require('express');
const { loadNuxt, build } = require('nuxt');
const { createTerminus } = require('@godaddy/terminus');

const config = require('../node.config.js');
const auth = require('./helpers/auth.js');
const logger = require('./helpers/logger.js');
const router = require('./api/routes/index.js');

// get services
const { sequelize } = require('./api/services/database.js');

function onSignal() {
  console.log(`Shutting down gracefully`);
}

function onShutdown() {
  console.log(`Server shutting down`);
}

async function createAppServer() {
  const app = express();
  
  app.use(bodyParser.json({
    limit: config.MAX_PAYLOAD_SIZE,
  }));

  // auth (with keycloak)
  app.use(auth.session);
  app.use(auth.keycloak.middleware());
  app.use(auth.keycloak.protect());
  app.use(auth.keycloak.middleware({ logout: `/logout` }));

  app.use(`/`, auth.userInfo);
  
  // setup router
  app.use(`/api`, logger.logRequest());
  app.use(`/api`, logger.logResponse());
  app.use(`/api`, router);

  // nuxt app setup
  const nuxtDevMode = config.IS_DEV;
  const nuxt = await loadNuxt(nuxtDevMode ? `dev` : `start`);

  app.use(nuxt.render);

  // (if dev mode: rebuild with hot-reloading)
  if (nuxtDevMode) {
    build(nuxt);
  }
  
  const server = http.createServer(app);
  createTerminus(server, {
    signals: [`SIGINT`, `SIGTERM`],
    timeout: 0,
    onSignal,
    onShutdown,
  });

  return server;
}

module.exports = createAppServer;
