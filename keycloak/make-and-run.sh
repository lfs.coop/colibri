VOLUME_NAME=colibri_mysql
NETWORK_NAME=colibri
IMAGE_NAME=colibri/keycloak

echo "=============="
echo "BUILDING IMAGE: ${IMAGE_NAME}"
echo "=============="
# if [[ "$(docker images -q ${IMAGE_NAME} 2> /dev/null)" == "" ]]; then
#   docker build . -f Dockerfile -t ${IMAGE_NAME}
# else
#   echo "Image '${IMAGE_NAME}' already built!"
# fi
docker build . -f Dockerfile -t ${IMAGE_NAME}

echo "==========================="
echo "CHECKING VOLUME AND NETWORK"
echo "==========================="
if [ ! "$(docker volume ls | grep -w ${VOLUME_NAME})" ];
  then
    echo  "creating ${VOLUME_NAME} volume !"
	  docker volume create --name ${VOLUME_NAME}
  else
    echo  "${VOLUME_NAME} volume already exists !"
fi

if [ ! "$(docker network ls | grep -w ${NETWORK_NAME})" ];
  then
    echo  "creating ${NETWORK_NAME} network ! "
    docker network create ${NETWORK_NAME}
  else
    echo  "${NETWORK_NAME} network already exists ! "
fi

if [ "$(docker ps -aq -f name=${VOLUME_NAME})" ]; then
  docker rm -f $(docker ps -aq -f name=${VOLUME_NAME})
fi

echo "============="
echo "RUNNING IMAGE: ${IMAGE_NAME}"
echo "============="
docker run -d --name ${VOLUME_NAME} \
  --restart unless-stopped \
  -p 127.0.0.1:3306:3306 \
  --net ${NETWORK_NAME} \
  -v ${VOLUME_NAME}:/var/lib/mysql \
  -e "MYSQL_DATABASE=keycloak" \
  -e "MYSQL_USER=${COLIBRI_MYSQL_USER:-default}" \
  -e "MYSQL_PASSWORD=${COLIBRI_MYSQL_PASSWORD:-default}" \
  -e "MYSQL_ROOT_PASSWORD=${COLIBRI_MYSQL_PASSWORD:-default}" \
  mysql:5.7.22

if [ "$(docker ps -aq -f name=keycloak)" ]; then
    docker rm -f $(docker ps -aq -f name=keycloak)
fi

docker run -d --name keycloak \
  -p 127.0.0.1:8080:8080 \
  --restart unless-stopped \
  --net ${NETWORK_NAME} \
  -e "DB_VENDOR=MYSQL" \
  -e "KEYCLOAK_USER=${COLIBRI_KEYCLOAK_USER:-default}" \
  -e "KEYCLOAK_PASSWORD=${COLIBRI_KEYCLOAK_PASSWORD:-default}" \
  -e "KEYCLOAK_LOGLEVEL=INFO" \
  -e "PROXY_ADDRESS_FORWARDING=true" \
  -e "DB_ADDR=${VOLUME_NAME}" \
  -e "DB_PORT=3306" \
  -e "DB_DATABASE=keycloak" \
  -e "DB_USER=${COLIBRI_MYSQL_USER:-default}" \
  -e "DB_PASSWORD=${COLIBRI_MYSQL_PASSWORD:-default}" \
  ${IMAGE_NAME}

docker exec -it keycloak sh keycloak/init.sh
