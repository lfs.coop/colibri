KCADM="/opt/jboss/keycloak/bin/kcadm.sh"
n=0
until [ $n -ge 5 ]
do
  ${KCADM} config credentials --server http://localhost:8080/auth --realm master --user "${KEYCLOAK_USER}" --password "${KEYCLOAK_PASSWORD}" && break
  n=$[$n+1]
  sleep 10
done

REALM_NAME=colibri
CLIENT_NAME=colibri
REALM_CLIENT_ROLE=${CLIENT_NAME}
ADMIN_ROLE=admin
ADMIN_USER=colibri
ADMIN_PASSWORD=colibri
ADMIN_EMAIL="${ADMIN_USER}@colibri.com"

# create default realm
${KCADM} create realms -s realm=${REALM_NAME} -s enabled=true

# create custom role on realm
${KCADM} create roles -r ${REALM_NAME} -s name=${REALM_CLIENT_ROLE} -s 'description=Base role for the app'

# create default client on realm
${KCADM} create clients -r ${REALM_NAME} -s clientId=${CLIENT_NAME} -o --fields id
CLIENT_ID=$(${KCADM} get clients -r ${REALM_NAME} -q clientId=${CLIENT_NAME} -F id --format csv --noquotes)
echo "Client ID: ${CLIENT_ID}"
# update client with correct redirectUris
${KCADM} update clients/${CLIENT_ID} -r ${REALM_NAME} -s publicClient=false -s authorizationServicesEnabled=true -s directAccessGrantsEnabled=true -s serviceAccountsEnabled=true -s "redirectUris=[\"*\"]"

# create admin role on client
${KCADM} create clients/${CLIENT_ID}/roles -r ${REALM_NAME} -s name=${ADMIN_ROLE} -s 'description=Admins are super-users on the client'
${KCADM} create clients/${CLIENT_ID}/roles -r ${REALM_NAME} -s name="anim-lead" -s 'description=Head of anim team'

# create admin user with custom realm role + admin client role
${KCADM} create users -r ${REALM_NAME} -s username=${ADMIN_USER} -s email=${ADMIN_EMAIL} -s enabled=true -s emailVerified=true
${KCADM} set-password -r ${REALM_NAME} --username ${ADMIN_USER} --new-password ${ADMIN_PASSWORD}
${KCADM} add-roles -r ${REALM_NAME} --uusername ${ADMIN_USER} --rolename ${REALM_CLIENT_ROLE}
${KCADM} add-roles -r ${REALM_NAME} --uusername ${ADMIN_USER} --cclientid realm-management --rolename view-users
${KCADM} add-roles -r ${REALM_NAME} --uusername ${ADMIN_USER} --cclientid ${CLIENT_NAME} --rolename ${ADMIN_ROLE}
${KCADM} add-roles -r ${REALM_NAME} --uusername ${ADMIN_USER} --cclientid ${CLIENT_NAME} --rolename "anim-lead"

# add service account roles
SERVICE_ACCOUNT_USERNAME="service-account-${CLIENT_NAME}"
${KCADM} add-roles -r ${REALM_NAME} --uusername ${SERVICE_ACCOUNT_USERNAME} --cclientid realm-management --rolename realm-admin
