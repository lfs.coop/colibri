#!/bin/bash


FILE=/usr/src/app/database/colibri.sqlite
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else 
    echo "$FILE does not exist."
    touch /usr/src/app/database/colibri.sqlite
	chmod u=rw /usr/src/app/database/colibri.sqlite
	yarn init-db

	sqlite3 $FILE "insert into websockets (host,port) \
         values (\"localhost\",8137);"
    sqlite3 $FILE "insert into anim_rig_libraries (name,owner,public,active) \
         values (\"shared_test\",\"lfs\",1,1);"

fi



yarn serve
