# Database structure and schemas

Colibri uses `sqlite` for database management. By default, the database is written as a file located at `./database/colibri.sqlite` (starting from the root of the repo).

Table and column names should be `snake_cased`.

Below is an explanation on the various tables in the database structure.

## Anim/rig tables

### Poses

**Name:** `poses`

**Description:** this table defines animation poses. The pose thumbnail is an image stored in the `client/static/content/poses` folder.

**Columns schema**

| Column  | Type    | Additional attributes      |
|---------|---------|----------------------------|
| `id`    | INTEGER | AUTOINCREMENT, PRIMARY KEY |
| `pose_title`  | TEXT    | NOT NULL           |
| `pose_json`  | TEXT    | NOT NULL                   |
| `creation_date`  | INTEGER | NOT NULL                   |
| `thumbnail_path` | TEXT    | NOT NULL                   |
| `applied_counter` | INTEGER    | NOT NULL                   |
