export default {
  "common": {
    "project": "project | projects",
    "lib": "lib | libs",
    "library": "library | libraries",
    "user": "user | users",
    "name": "name",
    "inactive": "inactive",
    "get": "get",
    "apply": "apply",
    "create": "create",
    "delete": "delete",
    "publish": "publish",
    "unpublish": "unpublish",
    "revive": "revive",
    "copy": "copy",
    "edit": "edit",
    "view": "view",
    "select": "Select",
    "approved": "Approved",
    "show-inactive": "show inactive",
    "no-results": "No results.",
  },
  "errors": {
    "create-lib": {
      "title": "Could not create the library",
      "content": {
        "SequelizeUniqueConstraintError": "Another lib with this name already exists!",
      },
    },
    "create-project": {
      "title": "Could not create the project",
      "content": {
        "params-already-taken": "Another project with the same name already exists!",
        "SequelizeValidationError": "These parameters are invalid for a project",
      },
    },
    "update-project": {
      "title": "Could not update the project",
      "content": {},
    },
    "create-websocket": {
      "title": "Could not create the websocket",
      "content": {
        "params-already-taken": "Another websocket with these params already exists!",
        "SequelizeValidationError": "These parameters are invalid for a websocket (check that port is more than 6000)",
      },
    },
    "pose-already-exists": "This name is already taken!",
    "error-occurred": "An error occurred",
  },
  "app-bar": {
    "menus": {
      "anim-rig": "anim/rig",
    },
    "switch-theme": "Switch theme",
    "switch-language": "Switch language",
    "log-out": "Log out",
  },
  "anim-rig": {
    "title": "Anim/rig",
    "owned-by": "owned by: {owner}",
    "bone": "bone | bones",
    "user-libs": {
      "title": "My lib | My libs",
    },
    "public-libs": {
      "title": "Shared lib | Shared libs",
    },
    "publish-lib": {
      "title": "Publish library",
      "content": "Are you sure you want to make the library '{name}' public?",
    },
    "unpublish-lib": {
      "title": "Unpublish library",
      "content": "Are you sure you want to make the library '{name}' private?",
    },
    "delete-lib": {
      "title": "Delete library",
      "content": "Are you sure you want to delete the library '{name}'?",
    },
    "revive-lib": {
      "title": "Revive library",
      "content": "Are you sure you want to revive the library '{name}'?",
    },
    "lib-page": {
      "pose-details": "Pose details",
      "create-pose": {
        "title": "Create pose",
        "content": "Create a new pose with the following name:",
        "from-blender-pose": "From the pose selected in Blender",
      },
      "copy-pose": {
        "title": "Copy pose",
        "content": "Copy this pose to one or multiple lib(s):",
      },
      "delete-pose": {
        "title": "Delete pose",
        "content": "Are you sure you want to delete the pose '{name}'?",
      },
      "revive-pose": {
        "title": "Revive pose",
        "content": "Are you sure you want to revive the pose '{name}'?",
      },
      "pose-type": "Pose type",
      "pose-types": {
        "pose": "Pose",
        "selection set": "Selection set",
        "animation": "Animation"
      },
      "get-pose": "get pose",
      "apply-pose": "apply pose",
      "get-animation": "get animation",
      "apply-animation": "apply animation",
      "flip": "flip",
      "apply-on-selected": "on selected bones",
      "import-at-current-frame": "import at current frame",
      "pick-poses": "Pick one or more libraries",
    },
  },
  "admin": {
    "title": "Admin",
    "projects": {
      "reader": "Reader | Readers",
      "writer": "Writer | Writers",
      "set-libraries": {
        "title": "Set project libraries",
        "content": "Pick the libraries you want in your project:",
      },
      "set-users": {
        "title": "Set project users",
        "content": "Set the users roles for this project:",
      },
      "view-libraries": {
        "title": "View project libraries",
        "content": "Libraries in this project:",
      },
      "delete": {
        "title": "Delete project",
        "content": "Are you sure you want to delete the project '{name}'?",
      },
      "revive": {
        "title": "Revive project",
        "content": "Are you sure you want to revive the project '{name}'?",
      },
    },
    "websockets": {
      "title": "websocket | websockets",
      "host": "host",
      "port": "port",
      "connect": "connect",
      "disconnect": "disconnect",
      "connected": "connected",
      "delete": {
        "title": "Delete websocket",
        "content": "Are you sure you want to delete the websocket {id}?",
      },
    },
    "users": {
      "title": "User | Users",
    },
  },
  "search": {
    "searching-for": "Searching for:",
    "only-public-libs": "only public libs"
  },
  "footer": {
    "source-code": "Source code",
  },
  "misc": {
    "create-new-project": "Create a new project",
  },
}
