export default {
  "common": {
    "project": "projet | projets",
    "lib": "bib. | bibs.",
    "library": "bibliothèque | bibliothèques",
    "user": "utilisateur | utilisateurs",
    "name": "nom",
    "inactive": "inactif",
    "get": "récupérer",
    "apply": "appliquer",
    "create": "créer",
    "delete": "supprimer",
    "publish": "publier",
    "unpublish": "rendre privé",
    "revive": "réactiver",
    "copy": "copier",
    "edit": "éditer",
    "view": "voir",
    "select": "Sélectionner",
    "approved": "Approuvée",
    "show-inactive": "montrer si inactif",
    "no-results": "Aucun résultat.",
  },
  "errors": {
    "create-lib": {
      "title": "Impossible de créer la bibliothèque",
      "content": {
        "SequelizeUniqueConstraintError": "Une autre bibliothèque avec le même nom existe déjà !",
      },
    },
    "create-project": {
      "title": "Impossible de créer le projet",
      "content": {
        "params-already-taken": "Un autre projet avec le même nom existe déjà !",
        "SequelizeValidationError": "Ces paramètres sont invalides pour un projet",
      },
    },
    "update-project": {
      "title": "Impossible de mettre le projet à jour",
      "content": {},
    },
    "create-websocket": {
      "title": "Impossible de créer la websocket",
      "content": {
        "params-already-taken": "Une autre websocket avec les mêmes paramètres existe déjà !",
        "SequelizeValidationError": "Ces paramètres sont invalides pour une websocket (vérifier que le port vaut plus que 6000)",
      },
    },
    "pose-already-exists": "Ce nom est déjà utilisé !",
    "error-occurred": "Une erreur est survenue",
  },
  "app-bar": {
    "menus": {
      "anim-rig": "anim/rig",
    },
    "switch-theme": "Changer le thème",
    "switch-language": "Changer la langue",
    "log-out": "Se déconnecter",
  },
  "anim-rig": {
    "title": "Anim/rig",
    "owned-by": "appartient à : {owner}",
    "bone": "os | os",
    "user-libs": {
      "title": "Ma lib | Mes libs",
    },
    "public-libs": {
      "title": "Lib partagée | Libs partagées",
    },
    "publish-lib": {
      "title": "Publier la bibliothèque",
      "content": "Êtes-vous sûr(e) de vouloir rendre la bibliothèque '{name}' publique ?",
    },
    "unpublish-lib": {
      "title": "Rendre la bibliothèque privée",
      "content": "Êtes-vous sûr(e) de vouloir rendre la bibliothèque '{name}' privée ?",
    },
    "delete-lib": {
      "title": "Supprimer la bibliothèque",
      "content": "Êtes-vous sûr(e) de vouloir supprimer la bibliothèque '{name}' ?",
    },
    "revive-lib": {
      "title": "Réactiver la bibliothèque",
      "content": "Êtes-vous sûr(e) de vouloir réactiver la bibliothèque '{name}' ?",
    },
    "lib-page": {
      "pose-details": "Détails de la pose",
      "create-pose": {
        "title": "Créer une pose",
        "content": "Créer une nouvelle pose avec le nom suivant :",
        "from-blender-pose": "A partir de la pose sélectionnée dans Blender",
      },
      "copy-pose": {
        "title": "Copier la pose",
        "content": "Copier cette pose dans une ou plusieurs bibliothèque(s) :",
      },
      "delete-pose": {
        "title": "Supprimer la pose",
        "content": "Êtes-vous sûr(e) de vouloir supprimer la pose '{name}' ?",
      },
      "revive-pose": {
        "title": "Réactiver la pose",
        "content": "Êtes-vous sûr(e) de vouloir réactiver la pose '{name}' ?",
      },
      "pose-type": "Type de pose",
      "pose-types": {
        "pose": "Pose",
        "selection set": "Selection set",
        "animation": "Animation"
      },
      "get-pose": "récupérer la pose",
      "apply-pose": "appliquer la pose",
      "get-animation": "récupérer l'anim",
      "apply-animation": "appliquer l'anim",
      "flip": "miroir",
      "apply-on-selected": "sur la sélection",
      "import-at-current-frame": "importer à la frame actuelle",
      "pick-poses": "Choisissez une ou plusieurs bibliothèques",
    },
  },
  "admin": {
    "title": "Admin",
    "projects": {
      "reader": "Lecteur | Lecteur",
      "writer": "Auteur | Auteurs",
      "set-libraries": {
        "title": "Éditer les bibliothèques du projet",
        "content": "Choisissez les bibliothèques que vous voulez dans ce projet :",
      },
      "set-users": {
        "title": "Éditer les utilisateurs du projet",
        "content": "Permissions des utilisateurs pour ce projet :",
      },
      "view-libraries": {
        "title": "Bibliothèques du projet",
        "content": "Bibliothèques assignées à ce projet :",
      },
      "delete": {
        "title": "Supprimer le projet",
        "content": "Êtes-vous sûr(e) de vouloir supprimer le projet '{name}' ?",
      },
      "revive": {
        "title": "Réactiver le projet",
        "content": "Êtes-vous sûr(e) de vouloir réactiver le projet '{name}' ?",
      },
    },
    "websockets": {
      "title": "websocket | websockets",
      "host": "hôte",
      "port": "port",
      "connect": "connecter",
      "disconnect": "déconnecter",
      "connected": "connectée | connectées",
      "delete": {
        "title": "Supprimer la websocket",
        "content": "Êtes-vous sûr(e) de vouloir supprimer la websocket {id} ?",
      },
    },
    "users": {
      "title": "Utilisateur | Utilisateurs",
    },
  },
  "search": {
    "searching-for": "Recherche :",
    "only-public-libs": "libs publiques uniquement"
  },
  "footer": {
    "source-code": "Code source",
  },
  "misc": {
    "create-new-project": "Créer un nouveau projet",
  },
}
