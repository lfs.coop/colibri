import { default as en } from './en.js';
import { default as fr } from './fr.js';

export const messages = { en, fr };
