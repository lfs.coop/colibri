import { SocketState } from 'colibri-sockets';

export const WEBSOCKET_STATE_COLORS = {
  default: `#aaaaaa`,
  [SocketState.DISCONNECTED]: `#aaaaaa`,
  [SocketState.CONNECTING]: `#ddaa11`,
  [SocketState.CONNECTED]: `#11dd11`,
  [SocketState.CLOSING]: `#ddaa11`,
  [SocketState.CLOSED]: `#dd1111`,
}
