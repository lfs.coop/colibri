export function capitalize(value) {
  if (!value) return ``
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
}

export function uppercase(value) {
  if (!value) return ``
  return value.toUpperCase()
}

export function formatDigit(value, length, fillChar) {
  if (!value) return ``
  if (typeof value !== `string`) value = value.toString()
  return fillChar.repeat(length - value.length) + value
}

export function formatTime(time) {
  const minutes = Math.floor(time / 60)
  const seconds = Math.floor(time % 60)
  return `${formatDigit(minutes, 2, `0`)}:${formatDigit(seconds, 2, `0`)}`
}

export function formatDate(date, locale = `en`) {
  if (typeof date === `string`) date = new Date(date)
  const options = { year: `numeric`, month: `long`, day: `numeric` }
  return new Date(date).toLocaleDateString(locale, options)
}
