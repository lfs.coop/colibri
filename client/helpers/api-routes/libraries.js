// -- READ
export function get(name, showInactive = false) {
  let url = `/anim-rig/libraries/${name}/`;
  if (showInactive) url += '?showInactive=true';
  return url;
}
