// -- CREATE
export function create() {
  return `/projects/`;
}

// -- READ
export function list() {
  return `/projects/`;
}

// -- UPDATE
export function updateProject({ id }) {
  return `/projects/${id}`;
}

export function updateProjectAnimRigLibraries({ id }) {
  return `/projects/${id}/anim-rig-libaries/`;
}

// -- DELETE
export function _delete({ id }) {
  return `/projects/${id}`;
}
