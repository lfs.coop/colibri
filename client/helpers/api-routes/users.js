// -- READ
export function list() {
  return `/users/`;
}

export function getCurrent() {
  return `/users/current`;
}
