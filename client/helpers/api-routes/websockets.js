// -- CREATE
export function create() {
  return `/websockets/`;
}

// -- READ
export function list() {
  return `/websockets/`;
}

// -- DELETE
export function _delete({ id }) {
  return `/websockets/${id}`;
}
