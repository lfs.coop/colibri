// -- READ
export function getData(id) {
  return `/anim-rig/poses/${id}/data`;
}
