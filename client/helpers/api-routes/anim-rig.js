// -- CREATE
export function createUserLibrary() {
  return `/anim-rig/libraries/`;
}

export function createPose() {
  return `/anim-rig/poses/`;
}

export function copyPoseToLibs({ id }) {
  return `/anim-rig/poses/${id}/copy`;
}

// -- READ
export function listAllLibraries() {
  return `/anim-rig/libraries/`;
}

export function listPublicLibraries() {
  return `/anim-rig/libraries/?group=public`;
}

export function listUserLibraries() {
  return `/anim-rig/libraries/?group=user`;
}

export function getLibrary({ name }) {
  return `/anim-rig/libraries/${name}/`;
}

// -- UPDATE
export function updateUserLibrary({ name }) {
  return `/anim-rig/libraries/${name}/?group=user`;
}

export function updatePublicLibrary({ name }) {
  return `/anim-rig/libraries/${name}/?group=public`;
}

export function updateLibraryFavorite({ name }) {
  return `/anim-rig/libraries/${name}/favorite`;
}

export function updatePose({ id }) {
  return `/anim-rig/poses/${id}/`;
}

export function updatePoseThumbnail({ id }) {
  return `/anim-rig/poses/${id}/thumbnail`;
}

export function updatePoseThumbnailMovie({ id }) {
  return `/anim-rig/poses/${id}/thumbnail-movie`;
}

// -- DELETE
export function deleteLibrary({ name }) {
  return `/anim-rig/libraries/${name}/`;
}

export function deletePose({ id }) {
  return `/anim-rig/poses/${id}/`;
}
