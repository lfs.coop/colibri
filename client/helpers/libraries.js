import _ from 'lodash';

export const POSE_TYPES_MAPPER = {
  'pose': 'POSE',
  'selection set': 'SELECTION_SET',
  'animation': 'ANIMATION',
};
export const POSE_ACTIONS = {
  'replace': 'REPLACE',
  'extend': 'EXTEND',
};

function computeTreeRec(tree, groups, key, flat) {
  if (!_.has(groups, key)) {
    return;
  }
  groups[key].forEach((rootLib) => {
    if (flat) {
      tree.push(rootLib);
      if (_.has(groups, rootLib.id)) {
        computeTreeRec(tree, groups, rootLib.id, flat);
      }
    } else {
      rootLib.children = [];
      if (_.has(groups, rootLib.id)) {
        computeTreeRec(rootLib.children, groups, rootLib.id, flat);
      }
      tree.push(rootLib);
    }
  });
}

export function computeTree(libs, flat = true) {
  const toIndent = [];
  let parent;
  let tree = libs.map((lib) => {
    if (lib.parent_lib === null) {
      lib.indent = 0;
    } else {
      parent = _.find(libs, [ `id`, lib.parent_lib ]);
      if (!_.isNil(parent) && _.has(parent, `indent`)) {
        lib.indent = parent.indent + 1;
      } else {
        toIndent.push(lib);
      }
    }
    return lib;
  });

  toIndent.forEach((lib) => {
    parent = _.find(libs, [ `id`, lib.parent_lib ]);
    if (!_.isNil(parent)) {
      lib.indent = parent.indent + 1;
    }
  });

  const groups = _.groupBy(tree, `parent_lib`);
  tree = [];
  computeTreeRec(tree, groups, `null`, flat);
  return tree;
}

export function findOpenNodesRec(tree, openList, queryId) {
  let foundInChildren = false;
  if (tree.children) {
    for (const child of tree.children) {
      foundInChildren = foundInChildren || findOpenNodesRec(child, openList, queryId);
    }
  }
  if (tree.id === queryId || foundInChildren) {
    openList.push(tree.id);
    return true;
  }
  return false;
}

export function findOpenNodes(tree, openList, queryId) {
  // (fills the openList in-place)
  for (const node of tree) {
    findOpenNodesRec(node, openList, queryId);
  }
}
