import { SocketState } from 'colibri-sockets';

export const onOpenHandler = (store) => (socket) => () => {
  socket.logger.success(`Opened socket`);
  store.commit(`websockets/setWebsocketState`,
               { name: socket.name, newState: SocketState.CONNECTED });
  
  store.commit('websockets/setSelectedWebsocket', socket.name);

  // ping back Blender client to get filename
  store.dispatch(`websockets/sendData`, {
    name: socket.name,
    data: { operator: `lfs.blender_ping`, },
    callback: () => (event) => {
      store.commit(`websockets/setWebsocketFilename`,
                   { name: socket.name, filename: event.data.filename });
    },
  });
}

export const onMessageHandler = (store) => (socket) => (event) => {
  const { data } = event;
  if (data.errored === true) {
    socket.logger.error(`Received event from ${socket.host}:${socket.port}:`, data);
  } else {
    socket.logger.info(`Received event from ${socket.host}:${socket.port}:`, data);
  }
}

export const onErrorHandler = (store) => (socket) => (error) => {
  socket.logger.error(JSON.stringify(error));
}

export const onCloseHandler = (store) => (socket) => (event) => {
  socket.logger.success(`Closed socket`);
  store.commit(`websockets/setWebsocketState`,
               { name: socket.name, newState: SocketState.CLOSED });
}

export const SOCKET_HANDLERS = (store) => ({
  onopen: onOpenHandler(store),
  onmessage: onMessageHandler(store),
  onerror: onErrorHandler(store),
  onclose: onCloseHandler(store),
});
