import Vue from 'vue';
import VueNotifications from 'vue-notification/dist/ssr.js';

// https://github.com/euvl/vue-notification/
Vue.use(VueNotifications);
