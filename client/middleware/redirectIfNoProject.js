export default function ({ store, redirect }) {
  // if no current active project, redirect to homepage
  if (!store.state.projects.currentProject) {
    return redirect('/');
  }
}
