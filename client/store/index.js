import * as usersApiRoutes from '~/helpers/api-routes/users.js';

export const state = () => ({})

export const actions = {
  async nuxtServerInit({ commit }, { $axios, req, error }) {
    // try and get the project id from the url
    const projectId = req.query.project;
    if (projectId)
      await commit(`projects/setCurrentProject`, { id: parseInt(projectId) });

    // load user info
    try {
      const user = await $axios.$get(usersApiRoutes.getCurrent());
      await commit(`users/setCurrentUser`, user);
    } catch (err) {
      error(err);
    }
  },
}
