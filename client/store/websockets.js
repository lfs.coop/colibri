import _ from 'lodash';
import shortid from 'shortid';

import * as websocketsApiRoutes from '~/helpers/api-routes/websockets.js';

import { ColibriSocketPool, SocketState, LOGGER_LEVELS } from 'colibri-sockets';
import { SOCKET_HANDLERS } from '~/helpers/websockets.js';

const SOCKETS_LOG_LEVEL = LOGGER_LEVELS.SUCCESS;

export const state = () => ({
  websockets: null,
  pool: null,
  selectedWebsocket: "",
})

export const mutations = {
  setWebsockets (state, { websockets }) {
    state.pool = new ColibriSocketPool();
    let socket;
    websockets.forEach((ws) => {
      socket = state.pool.add(
        { host: ws.host, port: ws.port,
          logLevel: SOCKETS_LOG_LEVEL,
          sendExitMessage: false, },
        SOCKET_HANDLERS(window.$nuxt.$store)
      );
      ws.name = socket.name;
      ws.state = SocketState.DISCONNECTED;
    });
    state.websockets = websockets;
  },
  addWebsocket (state, { id, host, port }) {
    const socket = state.pool.add(
      { id, host, port,
        logLevel: SOCKETS_LOG_LEVEL,
        sendExitMessage: false, },
      SOCKET_HANDLERS(window.$nuxt.$store)
    );
    state.websockets.push({ id, host, port, name: socket.name, state: SocketState.DISCONNECTED });
  },
  deleteWebsocket (state, { name }) {
    state.pool.unregister(name);
    const idx = _.findIndex(state.websockets, [ `name`, name ]);
    state.websockets.splice(idx, 1);
  },
  setWebsocketState (state, { name, newState }) {
    const websockets = _.cloneDeep(state.websockets);
    _.find(websockets, [ `name`, name ]).state = newState;
    state.websockets = websockets;
  },
  setWebsocketFilename (state, { name, filename }) {
    const websockets = _.cloneDeep(state.websockets);
    _.find(websockets, [ `name`, name ]).filename = filename;
    state.websockets = websockets;
  },
  setSelectedWebsocket (state, name) {
    state.selectedWebsocket = name;
  },
}

export const actions = {
  async loadWebsockets ({ commit }, { $axios }) {
    const websockets = await $axios.$get(websocketsApiRoutes.list());
    commit(`setWebsockets`, { websockets });
  },
  async connectWebsocket ({ state, commit }, { name }) {
    await state.pool.get(name).open();
    commit(`setWebsocketState`, { name, newState: SocketState.CONNECTING });
  },
  async disconnectWebsocket ({ state, commit }, { name }) {
    await state.pool.get(name).close();
    commit(`setWebsocketState`, { name, newState: SocketState.CLOSING });
  },
  async sendData ({ state }, { name, data, callback }) {
    if (callback) {
      const uid = shortid.generate();
      data.callback_uid = uid;
      callback = { event: uid,
                   func: callback,
                   stop: true };
    }
    await state.pool.get(name).send(data, callback);
  },
  clearPool({ state }) {
    if (state.pool !== null && !_.isArray(state.pool)) {
      state.pool.closeAll();
      state.pool.clear();
    }
  },
}
