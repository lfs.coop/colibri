import _ from 'lodash';
import * as projectsApiRoutes from '~/helpers/api-routes/projects.js';
import * as librariesHelpers from '~/helpers/libraries.js';

export const state = () => ({
  projects: null,
  currentProject: null,
  currentProjectLibsTree: null,
})

export const getters = {
  activeProjects: (state) => {
    if (state.projects === null) return null;
    return state.projects.filter(p => p.active);
  },
  allLibraries: (state) => {
    if (state.projects === null) return [];
    return _.sortBy(state.projects.reduce((acc, p) => {
      return [...acc, ...p.animRigLibraries];
    }, []), ['id']);
  },
  projectLibraries: (state) => {
    if (state.projects === null || state.currentProject === null) return [];
    const project = state.projects.find(p => p.id === state.currentProject);
    if (project === null) return [];
    return project.animRigLibraries;
  },
  isWriter: (state) => (username) => {
    if (state.projects === null || state.currentProject === null) return false;
    const project = state.projects.find(p => p.id === state.currentProject);
    return project.writers.includes(username);
  },
}

export const mutations = {
  setProjects (state, { projects }) {
    state.projects = projects;
    if (state.currentProject) {
      const p = state.projects.find(p => p.id === state.currentProject);
      if (p)
        state.currentProjectLibsTree = librariesHelpers.computeTree(p.animRigLibraries, false);
    }
  },
  addProject (state, project) {
    state.projects.push(project);
  },
  setProject (state, { id, project }) {
    const idx = _.findIndex(state.projects, [ `id`, id ]);
    state.projects.splice(idx, 1, project);
  },
  updateProject (state, { id, update }) {
    const idx = _.findIndex(state.projects, [ `id`, id ]);
    const project = _.cloneDeep(state.projects[idx]);
    for (const [key, value] of Object.entries(update))
      project[key] = value;
    state.projects.splice(idx, 1, project);
  },
  updateProjectLib (state, { projectId, libId, update }) {
    const pIdx = _.findIndex(state.projects, [ `id`, projectId ]);
    const project = _.cloneDeep(state.projects[pIdx]);
    const lib = _.find(project.animRigLibraries, [ `id`, libId ]);
    for (const [key, value] of Object.entries(update))
      lib[key] = value;
    state.projects.splice(pIdx, 1, project);
  },
  removeLibFromProject (state, { projectId, libId }) {
    const pIdx = _.findIndex(state.projects, [ `id`, projectId ]);
    const project = _.cloneDeep(state.projects[pIdx]);
    const lIdx = _.findIndex(project.animRigLibraries, [ `id`, libId ]);
    project.animRigLibraries.splice(lIdx, 1);
    state.projects.splice(pIdx, 1, project);
  },
  setCurrentProject (state, { id: projectId, vue }) {
    if (vue) {
      vue.$router.push(({
        path: vue.$route.path,
        query: { project: projectId }
      }));
    }
    state.currentProject = projectId;

    if (state.projects) {
      const p = state.projects.find(p => p.id === projectId);
      if (p)
        state.currentProjectLibsTree = librariesHelpers.computeTree(p.animRigLibraries, false);
    }
  },
  addCurrentProjectLib (state, { lib }) {
    if (state.currentProject === null) return;
    const pIdx = state.projects.findIndex(p => p.id === state.currentProject);
    const newProject = _.cloneDeep(state.projects[pIdx]);
    newProject.animRigLibraries.push(lib);
    state.projects.splice(pIdx, 1, newProject);
  },
  setCurrentProjectLib (state, { lib }) {
    if (state.currentProject === null) return;
    const pIdx = state.projects.findIndex(p => p.id === state.currentProject);
    const newProject = _.cloneDeep(state.projects[pIdx]);
    const libIdx = newProject.animRigLibraries.findIndex(l => l.name === lib.name);
    newProject.animRigLibraries[libIdx] = lib;
    state.projects.splice(pIdx, 1, newProject);
  },
  updateCurrentProjectLib (state, { id, update }) {
    if (state.currentProject === null) return;
    const pIdx = state.projects.findIndex(p => p.id === state.currentProject);
    const newProject = _.cloneDeep(state.projects[pIdx]);
    const lib = newProject.animRigLibraries.find(l => l.id === id);
    for (const [key, value] of Object.entries(update))
      lib[key] = value;
    state.projects.splice(pIdx, 1, newProject);
  },
  updateCurrentProjectLibPose (state, { libId, poseId, update }) {
    if (state.currentProject === null) return;
    const pIdx = state.projects.findIndex(p => p.id === state.currentProject);
    const newProject = _.cloneDeep(state.projects[pIdx]);
    const lib = newProject.animRigLibraries.find(l => l.id === libId);
    const pose = lib.poses.find(p => p.id === poseId);
    for (const [key, value] of Object.entries(update))
      pose[key] = value;
    state.projects.splice(pIdx, 1, newProject);
  },
};

export const actions = {
  async loadProjects ({ commit }, { $axios }) {
    const projects = await $axios.$get(projectsApiRoutes.list());
    commit(`setProjects`, { projects });
  },
}
