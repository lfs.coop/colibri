export const state = () => ({
  flip: false,
  applyOnSelected: false,
  importAtCurrentFrame: false,
})

export const mutations = {
  setFlip(state, { flip }) {
    state.flip = flip;
  },
  setApplyOnSelected(state, { applyOnSelected }) {
    state.applyOnSelected = applyOnSelected;
  },
  setImportAtCurrentFrame(state, { importAtCurrentFrame }) {
    state.importAtCurrentFrame = importAtCurrentFrame;
  },
};

export const actions = {
  updateFlip({ commit }, data) {
    commit(`setFlip`, data);
  },
  updateApplyOnSelected({ commit }, data) {
    commit(`setApplyOnSelected`, data);
  },
  updateImportAtCurrentFrame({ commit }, data) {
    commit(`setImportAtCurrentFrame`, data);
  },
}
