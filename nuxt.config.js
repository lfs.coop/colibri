import config from './node.config.js';
import pkg from './package.json';
import { messages } from './client/locales/index.js';
import colors from "vuetify/es5/util/colors";

export default {
  srcDir: `client/`,
  components: true,
  router: {
    base: config.ROUTER_BASE,
  },
  head: {
    title: `Colibri`,
    meta: [
      { charset: 'utf-8' },
      { name: `viewport`, content: `width=device-width, initial-scale=1` },
      { hid: `description`, name: `description`, content: `Meta description` }
    ],
    link: [{ rel: `icon`, type: `image/x-icon`, href: `/favicon.png` }],
  },
  build: {
    extend (config) {
      config.module.rules.push({
        resourceQuery: /blockType=i18n/,
        type: `javascript/auto`,
        loader: `@intlify/vue-i18n-loader`,
      })
    },
  },
  buildModules: [
    `@nuxtjs/svg`,
    `@nuxtjs/color-mode`,
    `@nuxtjs/vuetify`,
  ],
  modules: [
    `@nuxtjs/axios`,
    `@nuxtjs/style-resources`,
    [
      `nuxt-i18n`,
      {
        locales: [`en`, `fr`],
        defaultLocale: `en`,
        vueI18n: {
          fallbackLocale: `en`,
          messages,
          dateTimeFormats: {
            en: {
              short: {
                year: 'numeric', month: 'short',
              },
              long: {
                year: 'numeric', month: 'long', day: 'numeric',
              },
            },
            fr: {
              short: {
                year: 'numeric', month: 'short',
              },
              long: {
                year: 'numeric', month: 'long', day: 'numeric',
              },
            },
          },
        },
        detectBrowserLanguage: {
          useCookie: true,
          cookieKey: 'i18n_redirected'
        },
      }
    ],
  ],
  plugins: [
    { src: `~/plugins/notifications.js`, ssr: true },
  ],
  env: {
    APP_VERSION: pkg.version,
  },
  axios: {
    prefix: config.API_PREFIX,
    port: config.PORT,
    browserBaseURL: config.API_PREFIX,
  },
  css: [
    `~/styles/globals.scss`,
    `~/styles/base.scss`,
    `~/styles/theme-light.scss`,
    `~/styles/theme-dark.scss`,
  ],
  vuetify: {
    customVariables: ['~/assets/scss/variables.scss'],
    treeShake: true,
    theme: {
      dark: true,
      themes: {
        light: {
          primary: colors.amber.darken3,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent3,
          success: colors.green.accent3
        },
        dark: {
          primary: colors.amber.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent3,
          success: colors.green.accent3
        }
      }
    },
  },
  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return [`script`, `style`, `font`].includes(type);
      }
    }
  },
}